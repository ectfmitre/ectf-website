Technical Specifications
========================

This section contains all of the system requirements of the 2025 design. Your
design must meet all :term:`Functional Requirements` to pass :term:`Handoff` and should
attempt to meet all :term:`Security Requirements`

.. toctree::
    :maxdepth: 1
    :caption: Competition Specifications:

    functional_reqs
    security_reqs
    detailed_specs
