Functional Requirements
=======================
This section defines the functional requirements of your design. The
requirements are presented as a series of steps that define functionality your
system must implement. For each step, a Host Tool provided by the organizers
and run on a :ref:`2025_host_computer` will be invoked to interact with the
:ref:`2025_decoder`.

The functionality described in each step will be verified by the organizers
during :doc:`../handoff/index`, and you must follow **all** requirements to
advance to the :ref:`attack_phase`.

See :doc:`../system/reference_design` for details about how the
MITRE-provided Reference Design implements the Functional Requirements.

.. image:: ../../_static/2025\ eCTF\ High\ Level\ System.png
    :align: center
    :scale: 15 %

.. note::
    **The functional requirements described here are intentionally high level**.
    See the :doc:`detailed_specs` for detailed requirements including tool
    inputs, outputs, and environments.

.. _2025_build_sat:

Build Satellite TV System
-------------------------

.. figure:: ../../_static/Sat_Build.png
    :align: center
    :scale: 35 %

This build process is broken into multiple steps:

* :ref:`2025_build_environment`
* :ref:`2025_build_deployment`
* :ref:`2025_build_decoder`

Your team must implement each of these steps to create a full satellite TV system.

.. _2025_build_environment:

Build Environment
^^^^^^^^^^^^^^^^^

The Build Environment is where all of your project's dependencies are installed
including compilers, packages, and any other build tools you  may need. These
dependencies must be installed utilizing :doc:`../getting_started/docker`. This
step will be run once for each design, such that the successive build stages can
run as intended.

The environment is built by invoking ``docker build``, which will
build the docker image defined by the dockerfile. You may customize the
environment build process by modifying the dockerfile to install dependencies
for your build process.

This year, teams will have control over a docker container environment, which
will be used for building the Decoder firmware.

.. note::
    If you don't change the programming language of your design or add
    additional build tools, it is possible that the Build Environment step of
    the :doc:`../system/reference_design` will be sufficient for your
    design without modification.

.. _2025_build_deployment:

Build Deployment
^^^^^^^^^^^^^^^^

Next, you will create a deployment that represents an entire fabrication run of
components created by the satellite TV provider. During this step,
``gen_secrets.py`` is invoked by the user to generate initial data required for
building and provisioning the system.

The resulting files are known as the Global Secrets. They will be provided
to the subequent build processes of the :ref:`2025_decoder`, used to generate
:ref:`subscription updates <2025_update_subscriptions>` and will be passed
to the :ref:`2025_encoder`. The Global Secrets may include cryptographic key material,
seeds, entropy, or any other data needed to build your design. Global Secrets
should be treated as **read only** after generation; the Decoder and Encoder
components of the build process must not add new secrets to the Global Secrets.

Decoders and Encoders that use the same Global Secrets are known as a
"Deployment." Decoders and Encoders within a validly-built Deployment must be
able to work together as a functioning Satellite TV System. The behavior of
interaction between Encoders and decoders from different deployments is
undefined and will not be tested.

During the Attack Phase, one Deployment will be built for each scenario (see
:doc:`../flags/attack_flags`). Attackers will never have access to the Global
Secrets.


.. _2025_build_decoder:

Build Decoder
^^^^^^^^^^^^^

Once the environment and deployment have been built, you may now build the
Decoder, which is done by using the relevant
:doc:`Host Tool<../system/host_tools>`. Decoders and Encoders must be able to be
built in any order and may read from the Global Secrets as needed.

The Decoder will be built with the following command:

.. code-block::

    docker run -v ./decoder/:/decoder -v ./global.secrets:/global.secrets:ro -v ./deadbeef_build:/out -e DECODER_ID=0xdeadbeef build-decoder 

Which will place the decoder source code at ``/decoder`` in the container and the
global secrets at ``/global.secrets``. The Decoder ID will be defined with the 
environmental variable ``DECODER_ID``.

.. note::

    The Global Secrets are read-only, and may not be modified while building a
    Decoder! This means that there is no information flow from this step to
    the running Encoder

The built raw Decoder firmware should be placed at ``/out/max78000.bin``. You
may also place the ELF or other useful products in the ``/out`` directory for
your own debugging purposes, but they will not be used in the final design.


.. _2025_enc_func_reqs:

Encoder Functional Requirements
-------------------------------

Once the system is built, you may start the satellite system. In the full
deployment, this will include the :ref:`2025_uplink`, :ref:`2025_encoder`,
and :ref:`2025_satellite`, however during development, you may use the lighter
weight testing flow that connects your Encoder directly to the Decoder.

The Encoder is part of the pip-installable Python package ``ectf25_design``.
It is implemented as a function that accepts the raw frame to encode, the
channel it will be sent on, and the timestamp of the frame.

.. image:: ../../_static/2025\ Encoder.png
    :align: center
    :scale: 50 %

.. note::
    While the reference design uses the real time to generate the timestamps,
    this is for simplicity and your system **should not** expect or rely on
    being given time stamps that have any relation to the real time.



.. _2025_dec_func_reqs:

Decoder Functional Requirements
--------------------------------

With a functioning Decoder, the system should now be ready to accept
commands from the :doc:`../system/host_tools` to the
:ref:`2025_decoder`. The satellite tv system you design must respond to the
following three commands. Detailed requirements of these commands are listed
in the :doc:`detailed_specs`.

.. image:: ../../_static/Sat_Host_Tools.png
    :align: center
    :scale: 40 %

.. _2025_list_channels:

List Channels
^^^^^^^^^^^^^
The Decoder must be able to list the channel numbers that the Decoder has a
valid subscription for.

.. _2025_update_subscriptions:

Update Subscriptions
^^^^^^^^^^^^^^^^^^^^
To decode TV frame data, the Decoder must have a valid subscription for that
channel. The Decoder must be able to update it's channel subscriptions with a
valid update package. If a subscription update for a channel is received for a
channel that already has a subscription, the Decoder must only use the
latest subscription update. There is no subscription update associated with the
emergency broadcast channel (channel 0). Frames received on this channel must
always be properly decoded.

Subscriptions must be stored in a way that they persist on the decoder after it
loses power. This requirement applies to reboots that happen when the decoder is
listening for new commands from the host (as opposed to reboots that take place
while processing a command). For example, both lists in the following scenario
must return the same subscriptions.

1. Flash design
2. Subscribe to channel 1
3. List subscriptions
4. Power cycle
5. List subscriptions

.. _2025_decode_frame:

Decode Frame
^^^^^^^^^^^^
Decoding frames is the primary purpose of the Decoder.  The Decoder must
properly decode TV frame data from any channel that has a valid and active
subscription installed or for any emergency broadcast frames.
