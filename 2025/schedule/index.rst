Schedule
========

Phases
^^^^^^
* :ref:`design_phase` Teams design and build their own design
* :ref:`handoff`: Teams turn in their design to the eCTF organizers for testing
* :ref:`attack_phase`: Teams that have passed Handoff can earn points by
  attacking the designs of other teams in the Attack Phase

.. image:: ../../_static/eCTF\ Phases.png

.. _2025_important_dates:

Important Dates
^^^^^^^^^^^^^^^
* January 15: Competition Kickoff, :ref:`design_phase` begins
* February 26: :ref:`handoff` begins
* April 16: :doc:`../../about/scoreboard` closes, :term:`flags <flags>` no
  longer accepted
* April 25: :doc:`award_ceremony`

Design Phase Flags Due Dates
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
See :doc:`../flags/design_flags` for details.

* January 22: Read the Rules
* January 24: Boot Reference
* January 31: Initial Design Document
* February 7: Use Debugger
* February 14: Testing Service
* February 26: Attack the Reference Design

Workshop Schedule
^^^^^^^^^^^^^^^^^
The eCTF organizers will hold routine workshops to provide competitors with
information about different parts of the competition. Monitor the announcements
channel on :doc:`../../about/slack` for details.

* January 16: Rules & Reference Design Overview
* January 21: Boot Reference Design
* January 23: Design Document
* January 30: Docker Introduction
* February 4: Debugger Walkthrough
* February 6: Attack the Reference Design
* February 11: Testing Service
* February 14-16: :ref:`25_bci`
* February 20: Handoff Preparation

.. _25_bci:

BCI Training: Embedded Reversing and Exploitation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
eCTF Supporter Boston Cybernetics Institute returns this year to offer a free,
3-day course for all competitors. The course runs from 9am - 5pm on February
14th, 15th, and 16th.

This is a 3-day, hands-on course on reverse engineering and exploiting
embedded binaries. Exercises will be across multiple architectures (PowerPC,
MIPS, ARM, x86, and x64), and balances fundamentals with modern applications.
Exercises will focus on Linux programs and will include various firmware. After
completing this course, students will have the practical skills to begin
identifying vulnerabilities in embedded systems software and write exploits for
those vulnerabilities. 

A VM and Zoom information will be posted at the following link at 9am on
February 10th: 
`Link <https://bostoncyberneticsorg.sharepoint.com/:f:/s/CourseMaterials/EuHFUr2uevlAos7mValL660BbtjVwTYXJRMX8iJx3wBWmQ?e=qlWXCS>`_

Instructor bio: Jeremy Blackthorne (@0xJeremy) is an instructor and security
researcher at BCI, focused on vulnerability assessment and offensive cyber
operations. Before that, he was researcher in the Cyber System Assessments group
at MIT Lincoln Laboratory. Jeremy has published research at various academic and
industry conferences. He served as a rifleman and scout sniper in the U.S.
Marine Corps with three tours in Iraq. He has a BS in Computer Science from
University of Michigan-Dearborn and a MS in computer science from Rensselaer
Polytechnic Institute.

.. toctree::
    :maxdepth: 1
    :caption: Schedule and Events

    poster_session
    award_ceremony
