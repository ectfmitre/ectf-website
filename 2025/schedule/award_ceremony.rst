Award Ceremony
==============

The 2025 Award Ceremony will be held on April 25 in the Boston area. There will
be opportunities to apply for travel grants. More information will be announced
soon on :doc:`../../../about/slack` and updated here.
