Automated Testing Service
=========================

The eCTF team creates an Automated Testing Service to allow teams to verify that
their design meets the :doc:`../specs/functional_reqs`.

The testing service can be interacted with through :doc:`../../about/slack`.

The testing service is currently not online, check back with this page once the
service is online for instructions on how to submit your code for testing!

