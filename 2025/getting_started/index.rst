Getting Started
===============

This section of the docs will help with getting your machine set up to compete
in the 2025 eCTF! The purpose of this competition is to help develop your skills
in software, hardware, embedded systems, and security. Don't worry if some of
these are new to you; the point is to learn!

.. admonition:: Tip

    We recommend starting with reading :doc:`../../ectf_guide` to
    help your team get started off on the right foot.

Your team is responsible for developing the firmware to run on the MAX78000FTHR
development board. To allow for standardization of builds, firmware compilation
will be done utilizing :doc:`docker`, a tool that creates reproducible build
environments.  Example code for this competition, as well as examples provided
by Analog Devices, will be provided in C. Your team is welcome to use other
pre-approved languages (see :ref:`2025_allowed_languages`). **Any languages
outside of these will require approval from the organizers.**

Information on getting your computer set up with Docker and other tools is provided
in the sections below.

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    machine_setup
    boot_reference
    max78000_fthr
    docker
    openocd

The section below is **MacOS** only:

.. toctree::
    :maxdepth: 1

    daplink
