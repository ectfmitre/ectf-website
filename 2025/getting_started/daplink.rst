DAPLink
=======

DAPLink is the interface on the MAX78000FTHR responsible for providing debugging,
serial communications, and drag-and-drop programming.

If you are using **MacOS** the default DAPLink interface firmware will cause
the MAX78000FTHR to reset after every serial connection. This causes issues
for the eCTF. As such the organizers have provided a DAPLink binary that addresses
this issue.

Download and Installation
-------------------------

:download:`Download the DAPLink interface firmware here: DAPLink_NoReset.hex<../../_static/DAPLink_NoReset.hex>`

To flash this DAPLink interface to your development boards, utilize the current DAPLink
interface. Enter into DAPLink update mode on the MAX78000FTHR by holding down SW5 and
unplugging and replugging the USB connector on the board or toggling the power on the USB
hub.

.. figure:: ../../_static/max78000fthr_sw5.jpeg
    :align: center
    :scale: 15 %

    **SW5 on MAX78000FTHR**

The device should now show up as a removable media source named *MAINTENANCE*.
Simply drag and drop the file provided above to this drive to update the DAPLink interface.

.. figure:: ../../_static/DAPLink_Maintenance.png
    :align: center

    **DAPLink Maintenance**

