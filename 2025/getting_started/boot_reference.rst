Reference Design Boot Walkthrough
=================================

This section will help you with getting your machine running the eCTF :doc:`../system/reference_design`.
We will be discussing each of the steps to take you through the initialization!

First we will discuss the required components, programs, and anything else
that you need to succeed! Please refer to the :doc:`machine_setup` steps to make sure
everything is downloaded.

Depending on the Operating System of you machine, you will need to follow
different steps. This is because the commands are different for
:ref:`Linux<2025_boot_linux>` based machines and
:ref:`Windows<2025_boot_windows>` based machines.

.. _2025_boot_windows:

Windows
-------

Flashing the eCTF Bootloader
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Out of the box the board is a clean slate and we must flash the MITRE eCTF
insecure bootloader. To do so, you will first plug the board into the computer.
The board utilizes the *DAPLink* interface which means the board will show up as
a removable media source within the file explorer. (In the same way a flash
drive works) You will need to download the :doc:`../system/bootloader`.

:download:`Download the MITRE eCTF insecure bootloader here: insecure.bin. <../../_static/insecure.bin>`

After downloading, simply drag and drop the file into the removable drive which
appears in the file explorer. If you are unfamiliar, search "File Explorer" on
your computer and open the app. On the left sidebar, scroll down until you see
the "DAPLink" device. Click on that device to open it. Now open a new window
and, go to the folder you downloaded the bootloader to. Click and drag the
bootloader file to the DAPLink window.

If completed successfully, you should see a blue LED flash on the device.

Cloning the Reference Design
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Next, we need to download the Reference Design. To do so, we will need to create
a new folder. In your file explorer, create a new folder in a location in which
you will not forget where it is located. Click on the newly created folder. Next
at the top of the window, you will see a text box with your file path included
there. You will click on the textbox and highlight everything within that box.
Then you must type "powershell" and hit enter. This will bring up a powershell
window where you can type commands.

When you encounter multiple commands in this Walkthrough, they will be separated
by a new line. If there are multiple commands listed, enter commands
sequentially in order as most commands are dependent on previous commands. You
can copy and paste these commands directly into your PowerShell window!

To download the reference repo, run the following command.

.. code-block::

    git clone https://github.com/mitre-cyber-academy/2025-ectf-insecure-example


This will clone the insecure design to your local machine.

In your file explorer locate the root of the cloned files. It should contain folders named:
``decoder``, ``design``, ``frames``, and ``tools``. Open a new PowerShell window in this directory.

Creating a Python Virtual Environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We will begin to download the Python virtual environment. We will be entering commands in sequence
if you wish to learn more about these commands, they are located in the README file within the
project.

Enter the following commands into the terminal

.. code-block::

    python -m venv .venv --prompt ectf-example

This will set up a python virtual environment in which the necessary python libraries will be subsequently
installed. Note: the section labeled "ectf-example" is customizable. This will be the name of your virtual
environment so feel free to change this as you please!

.. code-block::

    .\.venv\Scripts\Activate.ps1

Take note of this command. This is the command you will enter each time you wish to activate the virtual
environment. You should see the prompt update with "ectf-example" or whatever you set as the ``--prompt`` argument.

.. code-block::

    # install the tools package
    python -m pip install .\tools\
    # install the design package as editable (updates to the package automatically are applied)
    python -m pip install -e .\design\

.. note::
    Files in the ``design`` directory should be updated as part of your team's secure
    design. Contents of the ``tools`` directory may not be modified by your team.

Locating the Serial Port
^^^^^^^^^^^^^^^^^^^^^^^^

The first step to flash the board with the reference design, is to know
where the data should be sent. The board uses serial communication meaning we must
know what port on our computer the device is plugged into. To locate the name of said port
you must open the Device Manager on your computer. There are a handful of ways of
doing so. You can press the Windows key and 'R' simultaneously and type 'devmgmt.msc'
then click OK. Another way is to press the windows key and in the search bar, look up
"Device Manager" and then click 'Open'.

You will see a long list of devices. The device manager allows us to monitor hardware
that is connected to our computer. Now that you have the device manager open, plug the
board into your computer. The list should refresh and a new item to the list should
have been made labeled 'Ports (COM & LPT)'. Click the arrow to bring up the drop down
menu. You will see a device listed there which is labeled as "COMxx" where the x's are
the respective port on your computer in which you plugged the device into. Take note
of this port each time you plug the device into your system as it will change frequently.

.. warning::
    You will need to have :doc:`docker` installed and working to progress.

Creating a Deployment
^^^^^^^^^^^^^^^^^^^^^

We will start by creating the shared secrets, which will be used by both the
Encoder and Decoder:

.. code-block::

    # Generate secrets, storing them in the file `global.secrets` and
    # defining the valid channels as 1, 3, and 4
    python -m ectf25_design.gen_secrets global.secrets 1 3 4 

Next, we will create the build environment, which will create a :doc:`docker`
image that will be used to build Decoders.

.. code-block::

    # Build the docker container, tagging it as `build-decoder` and using
    # the local directory `.\decoder` as the build context
    docker build -t build-decoder .\decoder

Building this image will take some time, roughly about 10 minutes. If it is
taking an abnormally long time, you can try closing the Docker application and
reopening it, or a full power cycle of your computer should resolve any issues.

.. admonition:: Tip

    In general, this Docker build step will only need to be run once. However,
    if you change dependencies, you will need to re-run this to rebuild the
    ``build-decoder`` image.


Building a Decoder
^^^^^^^^^^^^^^^^^^

With the environment built, you can now build a :ref:`2025_decoder`. This will
use the ``build-decoder`` Docker image, mounting local directories with the
Decoder source code, the global secrets, and an output directory for built
artifacts.

.. code-block::

    # Build a Decoder in a Docker container:
    #   `--rm` Delete the container when finished (remove when debugging to see output after completion)
    #   `-v .\decoder/:/decoder` Mount the local directory `decoder/` to `/decoder` in the container
    #   `-v .\global.secrets:/global.secrets` Mount the file `global.secrets` to `/global.secrets` in the container
    #   `-v .\deadbeef_build:/out` Mount the local directory `deadbeef.build/` to `/out` in the container
    #   `-e DECODER_ID=0xdeadbeef` Define the Decoder ID of the Decoder
    #   `build-decoder` Specify the tag of the Docker image to use
    docker run --rm -v .\decoder/:/decoder -v .\global.secrets:/global.secrets -v .\deadbeef_build:/out -e DECODER_ID=0xdeadbeef build-decoder 


Generating a Subscription Update
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Next, we will create a subscription update. We must change our location to the
project root folder so we have access to the secrets. To do so and generate a
new subscription, enter the commands:

.. code-block::

    # Generate a subscription:
    #    `global.secrets` Path to the global secrets file
    #    `deadbeef_c1.sub` Output subscription file
    #    `0xDEADBEEF` Decoder ID of target Decoder
    #    `32` Start timestamp
    #    `128` End timestamp
    #    `1` Channel ID of subscription
    python -m ectf25_design.gen_subscription global.secrets deadbeef_c1.sub 0xDEADBEEF 32 128 1


This will create the subscription file ``deadbeef_c1.sub``, which can be applied
to the the Decoder we created previously (ID `0xDEADBEEF``) to subscribe to
channel one from the timestamp 32 to the timestamp 128.

Flashing the Decoder Firmware
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Next we must flash the board. To do so, you must place the board into update
mode. Unplug your device, hold the small button labeled SW1 on the board, and
while still pressing the button, plug the board back in. You should see a blue
LED blinking approximately once per second, this means you have successfully
placed the device into update mode. Now that we are in update mode, we can now
flash the board. Enter the following command to flash the board:

.. admonition:: Tip

    Remember the port we identified earlier? Replace the section labeled ``COM3``
    with the port you located earlier.

.. code-block::

    # Flash a Decoder
    #    `.\deadbeef_build\max78000.bin` Path to the built Decoder
    #    `COM3` COM port to use to connect to the MAX78000FTHR board
    python -m ectf25.utils.flash .\deadbeef_build\max78000.bin COM3


.. _2025_boot_linux:

Linux
-----

Flashing the eCTF Bootloader
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Out of the box the board is a clean slate and we must flash the MITRE eCTF insecure bootloader. To do so, you will first plug the board into the computer.
The board utilizes the :doc:`DAPLink<daplink>` interface which means the OS will likely auto-mount it as a normal USB device. Left click the DAPLINK device,
and click "Open with File Manager". Then you will need to download the :doc:`../system/bootloader`.

:download:`Download the MITRE eCTF insecure bootloader here: insecure.bin. <../../_static/insecure.bin>`

Open a new window in the file explorer and navigate to the location of the downloaded bootloader file. Click and drag the file from this window into the DAPLINK window.

Cloning the Reference Design
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We first need to download the Reference Design. To do so, we will need to create a new
folder. In your file explorer, create a new folder in a location in which you will not
forget where it is located. Click on the newly created folder. This folder will be known as the
root folder. While within this folder, right click on the window, and select the option labeled "Terminal".
This will open a new terminal window with the file path of the root folder.

When you encounter multiple commands in this Walkthrough, they will be separated by a new line.
If there are multiple commands listed, enter commands sequentially in order as most commands are
dependent on previous commands. You can copy and paste these commands directly into your Terminal window!

To download the reference repo, run the following command.

.. code-block::

    git clone https://github.com/mitre-cyber-academy/2025-ectf-insecure-example


This will clone the repo so that we have access to the correct files.

Creating a Python Virtual Environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We will begin to download the Python virtual environment. We will be entering commands in sequence
if you wish to learn more about these commands, they are located in the README file within the
project.

Enter the following commands into the terminal

.. code-block::

    cd ./2025-ectf-insecure-example/
    python -m venv .venv --prompt ectf-example

This will set up a python virtual environment in which the necessary python libraries will be subsequently
installed. Note: the section labeled "ectf-example" is customizable. This will be the name of your virtual
environment so feel free to change this as you please!

.. code-block::

    . ./.venv/bin/activate

Take note of this command. This is the command you will enter each time you wish to activate the virtual
environment. You should see the prompt update with "ectf-example" or whatever you set as the ``--prompt`` argument.

.. code-block::

    python -m pip install ./tools/
    python -m pip install -e ./design/

.. note::
    Files in the ``design`` directory should be updated as part of your team's secure
    design. Contents of the ``tools`` directory may not be modified by your team.

Locating the Serial Port
^^^^^^^^^^^^^^^^^^^^^^^^

Before we can flash the firmware, we first need to locate which serial port the Decoder is connected to. Locating the port is as simple as copying
and pasting commands and locating differences. We are going to keep the board unplugged, then run the following command in any terminal to see the list of ports:

.. code-block::

    ls /dev/tty*

You will see all of the listed devices. Now plug the eCTF device into the computer and run the exact same command. You will compare the initial
output you got from the first execution to the second output of the command execution and find the different listed device. As the eCTF device was
the only change we made to the devices plugged in, the different device will be the port in which the eCTF is connected to. For example, the identified
file may look like:

.. code-block::

    /dev/ttyACM0
    /dev/ttyUSB0
    /dev/tty.usbmodem11302

* Take note of the name of the port as we will need it later

.. warning::
    You will need to have :doc:`docker` installed and working to progress.

Creating a Deployment
^^^^^^^^^^^^^^^^^^^^^

We will start by creating the shared secrets, which will be used by both the
Encoder and Decoder:

.. code-block::

    # Generate secrets, storing them in the file `global.secrets` and
    # defining the valid channels as 1, 3, and 4
    python -m ectf25_design.gen_secrets global.secrets 1 3 4 

Next, we will create the build environment, which will create a :doc:`docker`
image that will be used to build Decoders.

.. code-block::

    # Build the docker container, tagging it as `build-decoder` and using
    # the local directory `./decoder` as the build context
    docker build -t build-decoder ./decoder

Building this image will take some time, roughly about 10 minutes. If it is
taking an abnormally long time, you can try closing the Docker application and
reopening it, or a full power cycle of your computer should resolve any issues.

.. admonition:: Tip

    In general, this Docker build step will only need to be run once. However,
    if you change dependencies, you will need to re-run this to rebuild the
    ``build-decoder`` image.

Building a Decoder
^^^^^^^^^^^^^^^^^^

With the environment built, you can now build a :ref:`2025_decoder`. This will
use the ``build-decoder`` Docker image, mounting local directories with the
Decoder source code, the global secrets, and an output directory for built
artifacts.

.. code-block::

    # Build a Decoder in a Docker container:
    #   `--rm` Delete the container when finished (remove when debugging to see output after completion)
    #   `-v ./decoder/:/decoder` Mount the local directory `decoder/` to `/decoder` in the container
    #   `-v ./global.secrets:/global.secrets` Mount the file `global.secrets` to `/global.secrets` in the container
    #   `-v ./deadbeef_build:/out` Mount the local directory `deadbeef.build/` to `/out` in the container
    #   `-e DECODER_ID=0xdeadbeef` Define the Decoder ID of the Decoder
    #   `build-decoder` Specify the tag of the Docker image to use
    docker run --rm -v ./decoder/:/decoder -v ./global.secrets:/global.secrets:ro -v ./deadbeef_build:/out -e DECODER_ID=0xdeadbeef build-decoder 



Generating a Subscription Update
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Next, we will create a subscription update. We must change our location to the
project root folder so we have access to the secrets. To do so and generate a
new subscription, enter the commands:

.. code-block::

    # Generate a subscription:
    #    `global.secrets` Path to the global secrets file
    #    `deadbeef_c1.sub` Output subscription file
    #    `0xDEADBEEF` Decoder ID of target Decoder
    #    `32` Start timestamp
    #    `128` End timestamp
    #    `1` Channel ID of subscription
    python -m ectf25_design.gen_subscription global.secrets deadbeef_c1.sub 0xDEADBEEF 32 128 1


This will create the subscription file ``deadbeef_c1.sub``, which can be applied
to the the Decoder we created previously (ID `0xDEADBEEF``) to subscribe to
channel one from the timestamp 32 to the timestamp 128.


Flashing the Decoder Firmware
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Next we must flash the board. To do so, you must place the board into update
mode. Unplug your device, hold the small button labeled SW1 on the board, and
while still pressing the button, plug the board back in. You should see a blue
LED blinking approximately once per second, this means you have successfully
placed the device into update mode. Now that we are in update mode, we can now
flash the board. Enter the following command to flash the board:

.. admonition:: Tip

    Remember the port we identified earlier? Replace the section labeled
    ``usbmodem11302`` with the port you located earlier.

.. code-block::

    python -m ectf25.utils.flash ./deadbeef_build/max78000.bin /dev/tty.usbmodem11302

