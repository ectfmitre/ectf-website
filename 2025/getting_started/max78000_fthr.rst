MAX78000FTHR Board
==================

This years development platform is the Analog Devices MAX78000FTHR. The MAX78000FTHR is a capable
embedded platform.

.. image:: ../../_static/max78000fthr.jpg
    :align: center

The MAX78000 features:

- ARM Cortex-M4 Core
- RISC-V Core
- 512KB Flash Memory
- 128KB SRAM
- 16KB Cache
- Neural Network Accelerator
- Camera
- Audio Interface
- DAPLink Debugger

All documentation on the MAX78000FTHR is available at: https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/max78000fthr.html#eb-overview.

Some documentation to look over is the:

- `MAX78000FTHR Datasheet <https://www.analog.com/media/en/technical-documentation/data-sheets/MAX78000FTHR.pdf>`_
- `MAX78000FTHR Schematic <https://www.analog.com/media/en/technical-documentation/eval-board-schematic/max78000-fthr-schematic.pdf>`_
- `MAX78000 User Guide <https://www.analog.com/media/en/technical-documentation/user-guides/max78000-user-guide.pdf>`_

Some of these, the user guide especially, are very lengthy. Use these as a resource, but
full comprehension is not required.

The Analog Devices MSDK is the SDK (Software Development Kit) for the MAX78000FTHR. This SDK is
available at: https://github.com/Analog-Devices-MSDK/msdk.

Documentation on the MSDK is available at: https://analogdevicesinc.github.io/msdk/USERGUIDE/.

While there are installation instructions for the MSDK here, MSDK will be installed for the
2025 eCTF utilizing Docker. See the :doc:`docker` page for information on this installation.

A good place to start once you have all of the utilities installed is the Examples included
in the MSDK!

