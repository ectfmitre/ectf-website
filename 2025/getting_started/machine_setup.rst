Machine Setup
=============

To set up your machine for the eCTF, we require that you install the following list
of software:

- Git
- :doc:`docker`
- Python 3.11+

Docker
------

Docker supports installation through Docker Desktop on Windows, Linux, and MacOS.

Docker Desktop can be installed at:
https://www.docker.com/products/docker-desktop/

Specifics on how to use Docker can be found at :doc:`docker`.

Git
---

Git is an open-source version control system. It will allow your team to
collaborate on a single code base while managing a history of all the changes
you make. Git is required to submit your design for testing and progression to
the Attack Phase. This makes it easy for the organizers to download your code
for testing and allows your team to tag a specific version of your code you
want to submit.

All Platforms:

- https://git-scm.com/downloads

