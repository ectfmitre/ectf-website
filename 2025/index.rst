2025 eCTF
=========

Welcome to the 2025 eCTF!

This year, teams will design and implement a Satellite TV System solution. The
system must securely encode and decode satellite TV data streams while
protecting against unauthorized access to protected channels. Teams will design
both the :ref:`2025_encoder` that will encode TV frames to be broadcast over the
satellite in real-time to all teams as well as the hardware :ref:`2025_decoder`
responsible for securely decoding frames for display on the TV.

.. Figure:: ../_static/2025\ eCTF\ High\ Level\ System.png
    :align: center

    **High-Level Satellite TV System**

.. admonition:: Tip

    We recommend starting with reading :doc:`../ectf_guide` to
    help your team get started off on the right foot.

Thank You to our Sponsors!
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. Figure:: ../_static/2025\ eCTF\ Sponsors.png
    :align: center


The following pages will outline the details of the competition.


.. toctree::
    :maxdepth: 2
    :caption: Sections

    schedule/index
    getting_started/index
    system/index
    specs/index
    flags/index
    handoff/index


