System Overview
===============

.. toctree::
    :maxdepth: 1
    :caption: Competition Components:

    reference_design
    host_tools
    bootloader


Your team's design will consist of an :ref:`2025_uplink` and
:ref:`2025_Encoder` streaming data to a :ref:`2025_satellite`, a
:ref:`2025_host_computer`, and a hardware :ref:`2025_decoder`. The Decoder
firmware you design will securely decode TV frame data streamed over a
satellite's unidirectional data stream. The image below shows a high-level
overview of the system architecture.

.. Figure:: ../../_static/2025\ eCTF\ High\ Level\ System.png
    :align: center

    **High-Level Satellite TV System**

.. _2025_uplink:

Uplink
------

The Uplink component of the Satellite TV system is responsible for sending
frames to the :ref:`2025_satellite` to be broadcast to all Decoders. The
Uplink will first send raw frames to the :ref:`2025_encoder` and will then
forward the encoded frames to the satellite.

The Uplink is implemented as a Python script from the ``ectf25`` module,
networked with the Encoder and Satellite. The Uplink will be provided to teams
and **may not** be modified.


.. _2025_encoder:

Encoder
-------

Teams are responsible for implementing an Encoder design utilized by the
Satellite System. Generated TV data frames are passed through the Encoder before
being broadcast to to a listening :ref:`2025_decoder`. The Encoder is
implemented as a Python script from the ``ectf25_design`` module, networked with the
:ref:`2025_uplink` (you may choose to use a different language, see
:ref:`2025_allowed_languages`).

.. _2025_satellite:

Satellite
---------

The Satellite component of the Satellite TV System will run in
a cloud environment managed by the eCTF Organizers. The Satellite takes
encoded frames sent to it by the :ref:`2025_uplink` and forwards them to all
listening :ref:`TVs<2025_TV>` running on :ref:`Host Computers<2025_host_computer>`.

The Satellite is implemented as a Python script from the ``ectf25`` module,
networked with the Uplink. The Satellite will be provided to teams and **may not**
be modified.


.. _2025_host_computer:

Host Computer
-------------

The host computer is a general-purpose computer (i.e., your laptop or desktop)
used to communicate with the :ref:`2025_decoder` over a serial interface
through a number of :doc:`../system/host_tools`. These tools will be used to
initiate the various functionalalities of the Decoder device (see
:doc:`../specs/functional_reqs`) and to read back status messages and data. To simplify
the system, all Host Tools communicating with the device will be written by the
organizers.


.. _2025_TV:

TV
^^^^^^^^

The TV is the primary functionality running on the
:ref:`2025_host_computer`. It connects to the :ref:`2025_satellite` to receive
encoded packets, forwards them to the :ref:`2025_decoder` to be decoded, and
prints the decoded frames to the screen.

The TV is implemented as a Python script from the ``ectf25`` module.
The TV will be provided to teams and **may not** be modified.


.. _2025_decoder:

Decoder
-------

The main focus of your team's work is on the Decoder device. A Decoder is
implemented on an Analog Device MAX78000FTHR development board connected to a
:ref:`2025_host_computer` over a USB-serial interface. The Decoder will be used to
decode data being live streamed over a uni-directional communications link across
multiple channels. The Decoder is responsible for maintaining active
subscriptions and correctly decoding data frames on subscribed channels.

Development Resources
---------------------

Teams will be provided the following resources:

- **3 un-keyed MAX78000FTHR board (the development boards)**
    - These boards will be used for the development of your design. Instructions
      to run the host tools on a local computer to test the
      entire system using the physical hardware are found in :doc:`host_tools`. These devices will not be able
      to run :ref:`attack_phase` designs provisioned by the eCTF
      organizers. However, the development microcontrollers can be used to
      practice attacks against designs in the Attack Phase that are compiled
      locally by the team from source.

- **3 keyed MAX78000FTHR boards (the attack boards)**
    - Boxes have small red stickers to indicate that they contain provisioned
      attack boards
    - These boards will be used for the Attack Phase to securely the load other
      teams' designs that will be provided by the eCTF organizers. These devices
      are configured for use in the Attack Phase and therefore will be unusable
      during the Design Phase.

