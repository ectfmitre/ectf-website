Host Tools
==========

The Host Tools allow teams to have a standard interface to the system functionality described in
:doc:`../specs/functional_reqs`.  These include tools to build the system as outlined in :ref:`2025_build_sat`
(see :ref:`2025_build_tool_calls` below) as well as tools to interact with running decoders as outlined
in :ref:`2025_dec_func_reqs` (see :ref:`2025_sat_tool_calls` below).

Using the eCTF Tools
--------------------
In order to run the eCTF Tools, you must first ensure that you have installed
all of the required packages (ideally into a virtual environment).  You can
install packages from the included `pyproject.toml` file in the root of the
design directory. This file should not be modified.

.. code-block:: bash

    # Create a virtual environment in the root of the design
    cd <example_root>
    python -m venv .venv --prompt ectf-example
    # enable virtual environment
    . ./.venv/bin/activate
    # powershell users will instead need to run:
    # . .\.venv\Scripts\Activate.ps1
    # Install the host tools
    python -m pip install tools/
    # Install the host design elements as an editable module
    python -m pip install -e design/

.. _2025_build_tool_calls:

Build Tool Reference Calls
--------------------------

The build tools are a set of scripts created by the organizers that execute the
required build stages as outlined in the :doc:`../specs/functional_reqs`.

The build tools will be utilized by the organizers when testing and provisioning
your design. As such, your design **must** build with these included tools.

These tools **cannot** be modified by your team and must work as included with
the example design.

The build tools are to be executed in a Docker image.

.. figure:: ../../_static/Sat_Build.png
    :align: center
    :scale: 35 %


Build Deployment
^^^^^^^^^^^^^^^^
This will generate shared secrets for a given deployment. This step assumes that you have
installed the host design elements in your current python environment. As an example, the
below command generates secrets to a directory called 'secrets'. This directory will be
referenced in future instructions, but can be changed at the user's discretion.

.. code-block:: bash

    mkdir secrets/
    python -m ectf25_design.gen_secrets secrets/secrets.json 1 3 4


Build Decoder
^^^^^^^^^^^^^
The Decoder can be built after the secrets have been generated, allowing the build to mount
the shared secrets directory as a volume to the build container.

The below example generates a docker image called 'decoder' and uses it to run a container
where a Decoder build with Device ID 0xdeadbeef.  The build outputs of this command are copied
to the directory "build_out".

Example invocation:

.. code-block:: bash

    cd <design_root>/decoder
    docker build -t decoder .
    docker run --rm -v ./build_out:/out -v ./:/decoder -v ./../secrets:/secrets -e DECODER_ID=0xdeadbeef decoder

.. _2025_update_tool:

Flashing Tool
^^^^^^^^^^^^^
Flashing the MAX78000 is done through the :doc:`bootloader`. You will need to
initially flash the bootloader onto the provided hardware.

The eCTF bootloader needs to be in update mode to be flashable. The bootloader
is in update mode when the onboard LED is flashing blue. This mode can be
entered by holding the bootloader button while resetting the board.

.. code-block::

    python -m ectf25.utils.flash -h
    usage: ectf25.utils.flash [-h] infile port

    positional arguments:
    infile      Path to the input binary
    port        Serial port

    options:
    -h, --help  show this help message and exit

Example invocation:

.. code-block:: bash

    python -m ectf25.utils.flash ./build_out/max78000.bin /dev/tty.usbmodem11302


.. _2025_sat_tool_calls:

Satellite Tool Reference Calls
------------------------------

All host tools are provided by the organizers and create simplistic interfaces
for communication between the :ref:`2025_host_computer` and the
:ref:`2025_decoder`. These tools define the uniform interface utilized
to meet the technical requirements on the AP. See :ref:`2025_decoder_interface` for the
details of the protocol of the interface the Host Tools use to communicate with
the Decoder.


.. image:: ../../_static/Sat_Host_Tools.png
    :align: center
    :scale: 35 %


List Tool
^^^^^^^^^
The list tool applies the required list channels functionality from the Satellite TV Decoder system.

.. code-block::

    python -m ectf25.tv.list -h
    usage: ectf25.tv.list [-h] port

    List the channels with a subscription on the Decoder

    positional arguments:
    port        Serial port to the Decoder

    options:
    -h, --help  show this help message and exit


Example Utilization:

.. code-block:: bash

    python -m ectf25.tv.list /dev/tty.usbmodem11302

Subscription Update Tool
^^^^^^^^^^^^^^^^^^^^^^^^

The subscription update tool applies the required subscription update functionality from the
Satellite TV Decoder System.  The subscription update tool takes in an encoded update packet
(in the form of a `.bin` file) and sends it to the Decoder.  Note that subscription update
packets can be generated using Secure Facility Docker ENTRYPOINT.

.. code-block::

    python -m ectf25.tv.subscribe -h
    usage: ectf25.tv.subscribe [-h] subscription_file port

    Updates a Decoder's subscription.

    positional arguments:
    subscription_file  Path to the subscription file created by ectf25_design.gen_subscription
    port               Serial port to the Decoder

    options:
    -h, --help         show this help message and exit

Example Utilization:

.. code-block:: bash

    python -m ectf25.tv.subscribe subscription.bin /dev/tty.usbmodem11302

.. _2025_end_to_end_infrastructure:

Running the Satellite and Encoder
---------------------------------
To run all of the infrastructure, you will need to first start the uplink. Then, in a
separate terminal window, start the satellite. And finally, start a ``TV`` instance
for every Decoder being tested.

Uplink
^^^^^^

The uplink is the component of the Satellite TV system responsible for sending encoded
frames to the satellite. It will use the Encoder from your design to encode frames.

.. code-block::

    python -m ectf25.uplink -h
    usage: __main__.py [-h] secrets host port channels [channels ...]

    positional arguments:
    secrets     Path to the secrets file
    host        TCP hostname to serve on
    port        TCP port to serve on
    channels    List of channel:fps:frames_file pairings (e.g., 1:10:channel1_frames.json
                2:20:channel2_frames.json)

    options:
    -h, --help  show this help message and exit

Example Utilization:

.. code-block:: bash

    python -m ectf25.uplink ./decoder/secrets/secrets.json localhost 2000 1:10:frames/x_c1.json

Satellite
^^^^^^^^^

The satellite is responsible for broadcasting all frames received from the uplink to all
listening TVs on the host computer.

.. code-block::

    python -m ectf25.satellite -h
    usage: satellite.py [-h] up_host up_port down_host channels [channels ...]

    positional arguments:
    up_host     Hostname for uplink
    up_port     Port for uplink
    down_host   Hostname for downlink
    channels    List of channel:down_port pairings (e.g., 1:2001 2:2002)

    options:
    -h, --help  show this help message and exit

Example Utilization:

.. code-block:: bash

    python -m ectf25.satellite localhost 2000 localhost 1:2001

TV
^^

The TV is responsible for sending encoded frames received from the satellite to a
decoder connected to the host computer and returning the decoded result.

.. code-block::

    python -m ectf25.tv.run -h
    usage: ectf25.tv.run [-h] [--baud BAUD] sat_host sat_port dec_port

    positional arguments:
    sat_host     TCP host of the satellite
    sat_port     TCP port of the satellite
    dec_port     Serial port to the Decoder

    options:
    -h, --help   show this help message and exit
    --baud BAUD  Baud rate of the serial port

Example Utilization:

.. code-block:: bash

    python -m ectf25.tv.run localhost 2001 /dev/tty.usbmodem11302
