Attack Phase Flags and Scenarios
================================

.. admonition:: Tip

  **Understanding the contents of this page is critical to properly securing your
  design**. If something is unclear, please reach out to the organizers on
  :doc:`../../about/slack` to clarify.

.. figure:: ../../_static/2025\ eCTF\ Attack\ Setup.png
    :align: center


.. _2025_scenarios:

Attack Phase Scenarios
----------------------
Attackers will have access to a number of different subscription updates for a
:ref:`2025_decoder`. Each setup represents a realistic scenario that will test a
design's success at meeting one or more :doc:`../specs/security_reqs` by
embedding :ref:`2025_attack_flags` into different parts of the setup.

This year, the Attack Phase scenarios use a single
:ref:`deployment<2025_build_deployment>`, with a single satellite transmitting
across five channels. The images below summarize the flags, scenarios, relevant
security requirements, and deployments.

.. _2025_expired_subscription:

.. figure:: ../../_static/2025\ eCTF\ Flag\ Expired\ Subscription.png
    :align: center

.. _2025_pirated_subscription:

.. figure:: ../../_static/2025\ eCTF\ Flag\ Pirated\ Subscription.png
    :align: center

.. _2025_no_subscription:

.. figure:: ../../_static/2025\ eCTF\ Flag\ No\ Subscription.png
    :align: center

.. _2025_recording_payback:

.. figure:: ../../_static/2025\ eCTF\ Flag\ Recording\ Playback.png
    :align: center

.. figure:: ../../_static/2025\ eCTF\ Flag\ Pesky\ Neighbor.png
    :align: center

.. _2025_attack_flags:

Attack Phase Flags
------------------
During the Attack Phase, teams will test the security of other teams' designs by
attempting to capture Attack Phase :term:`Flags<flags>`. Each flag represents proof on
an attacker's ability to compromise one or more security requirements of a
design.

.. list-table::
    :header-rows: 1

    * - Flag
      - Format
      - Description
    * - Expired Subscription
      - ``ectf{expired_*}``
      - Read frames from a channel you have an expired subscription for
    * - Pirated Subscription
      - ``ectf{pirate_*}``
      - Read frames from a channel you have a pirated subscription for
    * - No Subscription
      - ``ectf{nosub_*}``
      - Read frames from a channel you have no subscription for
    * - Recording Playback
      - ``ectf{recording_*}``
      - Read frames from a recorded channel you currently have a subscription for, but didn't at the time of the recording
    * - Pesky Neighbor
      - ``ectf{neighbor_*}``
      - Spoof the signal of the satellite to cause your neighbor's Decoder to decode your frames instead
