Documentation Points
====================

Good documentation will be rewarded to discourage security-by-obscurity. “Good
documentation” includes clear and well-commented code, useful descriptions of
modules/functions/classes, a clear and accurate :doc:`design_doc`, and other
documents that clearly describe how to read or approach the entire code base.

We are not looking for lengthy documents that describe your implementation in
excruciating detail. A concise and clear *README.md*, including a brief
rationale for your security features, combined with well-structured and
well-commented code is sufficient for full points. Quality is valued over
quantity.

The maximum number of points that can be scored for documentation is equal to
the value of an uncaptured :doc:`Attack Phase Flag <attack_flags>` scored on the
last day of the competition (the exact value varies depending on when teams
enter the :ref:`attack_phase`, see
:doc:`../../about/scoring_system`), with the actual amount being a percentage of
that maximum:

- **Max** - Exemplary documentation, comments, and code structure; clear and
  easy to understand
- **75%** - Good comments and high-level documentation
- **50%** - Good comments, but lack of clear high-level documentation
- **25%** - Confusing code and little or no actual documentation
- **0%** - Confusing or deceptive comments and documentation

Points for documentation will not be awarded until the end of the Attack Phase.
Honest feedback on documentation from other teams as well as technical experts
from MITRE and the competition sponsors will be solicited and factored into the
final point determination.

