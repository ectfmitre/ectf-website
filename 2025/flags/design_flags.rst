Design Phase Flags
==================

During the :ref:`design_phase`, teams can show that they are staying
on track and earn some points by submitting Design Phase Flags. Teams
will earn full points for submitting flags by the deadline. Flags
submitted after the deadline will earn half points.


.. list-table::
    :header-rows: 1

    * - Milestone
      - Flag Format
      - Due Date
      - Points
      - Description
    * - Read Rules
      - ``ectf{readtherules_*}``
      - January 22
      - 100
      - If you read **all** the rules, you'll know
    * - Boot Reference Design
      - ``ectf{boot_*}``
      - January 24
      - 100
      - Provision and boot the :doc:`../system/reference_design` to receive
        a flag
    * - Design Document
      - ``ectf{designdoc_*}``
      - January 31
      - 100
      - Submit an initial :doc:`design_doc` to your team channel
    * - Debugger
      - ``ectf{debugger_*}``
      - February 7
      - 100
      - Show that you can use a debugger. See :doc:`debugger_flag` for more information
    * - Testing Service
      - ``ectf{testing_*}``
      - February 14
      - 100
      - Use the :term:`Testing Service` and read the results.
    * - Attack the Reference Design
      - See: :ref:`2025_attack_flags`
      - February 21
      - 100
      - Capture flags by attacking a deployment of the :doc:`../system/reference_design`.
        More information about capturing these flags will be released soon.
    * - Final Design Submission
      - ``ectf{handoff_*}``
      - April 16
      - 1,000
      - Pass :doc:`../handoff/index` to earn points and enter the
        :ref:`attack_phase`
    * - Bug Bounty
      -
      - April 16
      - Up to 200
      - See :ref:`2025_bug_bounty` below

.. _2025_bug_bounty:

Bug Bounty
----------

If your team happens to find a bug in the reference design, you can earn points
for it! Your team will receive 100 points for each bug found, and another 100
points if you submit a corresponding fix. If multiple teams find the same bug,
points will be distributed on a first come, first serve basis.

Sometimes whether an issue is truly a bug (or a feature!) is a matter of opinion
- the eCTF organizers reserve the right to reject bug reports for trivial issues
and combine multiple similar reported bugs into one. Submitted bugs will be
accepted if there is a violation of the functional requirements in the reference
design that prevents it from working correctly.

Submissions for typos or clarifications on documentation provided by the
organizers will not be considered for additional points, although we appreciate
being notified about these mistakes so we can make the appropriate edits and
provide further explanation where it is necessary.

Please submit Bug Bounty requests through your private team channel on
:doc:`../../about/slack` tagging ``@organizers``.
