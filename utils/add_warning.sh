#!/bin/sh

# Adds warning text block to the 3rd line of every *.rst file in a directory specified by an argument. Recursive
# Example: ./utils/add_warning.sh ./past_competitions

find $1 -name \*.rst -print0 | xargs -0 -n 1 sed -i '' '3i\
\
.. warning::\
\
    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY\
'
