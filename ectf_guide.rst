Start Here
==========
If you are new to the eCTF or embedded security, it can be difficult to know
where to get started and what to focus on. The breadth and depth of embedded
security is massive and with an open-ended challenge like "design a secure
system," it's easy to get overwhelmed.

For many competitors, the eCTF is also their first large-scale design and
implementation project. The scope and open-ended nature of the competition can
lead many teams into pitfalls if they attempt to approach it in a similar way
that you might when solving other, smaller school or personal projects.

To keep your team on track, the eCTF organizers are providing a step-by-step
path towards completing the Design Phase. This won't describe *how* you should
design your system, only the steps you should follow to give you the best shot
at completing your design without becoming overwhelmed.

As always, if you have questions, feel free to reach out to the organizers on
:doc:`../../about/slack`, tagging them with ``@organizers``.

.. admonition:: Quote

    "I had no security experience prior to this competition. The learning curve
    was HUGE and I LOVED that! I was forced to learn so much. I loved doing the
    research, designing and implementing the secure system, and reviewing and
    attacking other teams' designs. It was a blast!" - Former eCTF Competitor


0. Get to Know Your Team
^^^^^^^^^^^^^^^^^^^^^^^^
Some teams may know each other before the competition starts, while it may be
the first time other teams have met. Either way, it's important to get to know
each other, as you will be working together as a team for the next months.

Many engineering students assume that all work on large projects is technical,
hands-on-keyboards work. However, the larger the project, the more non-technical
work is required to coordinate and keep the project on track. 

This means that there are a number of roles that are helpful to complete the
competition, even for team members with no coding experience. Your team may find
it helpful to outline explicit roles for each team member, as that can help
create structure and clarify responsibility. Without roles, it can be easy for
the work to be unevenly distributed, overburdening some members and leaving out
others. This is especially true with larger teams.

To help, we suggest a number of roles, many based off of the way Delaware Area
Career Center (DACC) has structured their team:

* Project Lead: Manages the project and team
* Task Manager: Keeps the task tracker (Trello, Jira, etc.) up to date
* Lead Developer: Responsible for the code development and code reviews
* Secure Designers: Creates the cryptographic and protocol designs to meet the
  security requirements
* Developers: Developing the secure design
* Red Teamers: Focus on preparing attacks for the Attack Phase and finding
  vulnerabilities in their own team's design before Handoff
* Documenters: Takes notes on meetings and keeps the design document up to date
  with the latest design

Your team does not need to follow the exact roles and team members can be
assigned multiple roles. *It is crucial to show grace and respect while deciding
team roles, as it is important for everyone to feel that they got value from the
competition.* For more about how DACC structures their team, see team advisor
Eli Cochran's `video for the 2022 NICE K-12 Conference
<https://www.youtube.com/watch?v=Zck-9_1LfHo>`_


1. Understand the Challenge
^^^^^^^^^^^^^^^^^^^^^^^^^^^
After you organize your team, the first priority is to understand what is
being asked of you. In the eCTF, you must design a system that meets a series
of both :term:`Functional Requirements` that describe *how your design must
work* and :term:`Security Requirements` that describe *what your design must
protect against*.

It is critical that you understand these requirements, as they will make or
break your design. Failing to meet Functional Requirements means that you won't
be able to pass :term:`Handoff` to enter the :term:`Attack Phase`. Failing to
meet Security Requirements means that your design likely won't earn many
:term:`Defensive Points`. This means that even though the eCTF is a security
competition, *the functionality of your design is actually more important to
your success than the security is.*

Spend time getting to know the :doc:`../index` early, as it will save you time
and heartache later in the competition when you realize you missed an important
rule.

If you ever have any questions, please reach out to the organizers on
:doc:`../../about/slack`, tagging them with ``@organizers``. 


.. admonition:: Tip

    You cannot pass :term:`Handoff` to move into the :term:`Attack Phase` unless
    you meet all :term:`Functional Requirements`, so make sure your efforts to
    secure your design don't undermine the system's functionality!


2. Understand the Reference Implementation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To get teams started, the eCTF organizers have provided a
:term:`Reference Design` that implements the basic functionality of the
challenge.

This year, in addition to the reference design, we have included a :term:`Crypto
Interface`. This interface automatically compiles in the `wolfCrypt
<https://www.wolfssl.com/documentation/manuals/wolfssl/chapter10.html>`_
library, providing a number of well-vetted implementations of common
cryptographic algorithms. To make it even easier to use, the eCTF organizers
have provided an easy-to-use interface for symmetric encryption and decryption
and hashing. You don't necessarily need to understand how the library and
functions work, but you should understand how to use the provided interfaces.


3. Outline Your Design
^^^^^^^^^^^^^^^^^^^^^^

Once your team has a grasp on the requirements of the competition, you will need
to design your solution to meet the :term:`Security Requirements`. This secure
design is one of the most important parts of the Design Phase, as it will
determine the security of your submission.

However, many teams get stuck trying to make the perfect design and run out of
time to actually implement it. To avoid this, it is crucial to have a simple
design that you can focus on implementing first before you attempt to finish the
full, complex design. Aim for a design that is easy to implement and that you
think will secure at least a flag or two, even if it leaves other flags
vulnerable. Aiming for this before completing your full design allows your team
flexibility during :ref:`handoff` to trade off how strong you
anticipate your design is versus how much time you will have in the Attack
Phase.

Simple is often better in protocol design, as overly complex protocols can make
it difficult to fully understand its security and make it easy for
implementation mistakes to undermine the design. For teams who don't have
experience implementing cryptographic protocols on embedded systems, *we
strongly recommend using only the symmetric encryption and hash algorithms
implemented by the* :term:`Crypto Interface` *in your basic design.*

**Remember, you cannot move to the Attack Phase until you submit a function
design that implements some amount of non-trivial security, so start small and
build incrementally rather than shooting for the stars right away.**

An additional benefit of starting with a simple design is that you will
inevitably have to make changes to your design as you run into implementation
constraints. Making tweaks to accommodate these will be much easier on a simple
design than it would be on a complex one.

Once you have completed your basic design, the developer team can begin
implementing it while the design team completes their full design. The full
design does not need to be substantially more complex, but it should aim to be
more secure against the full set of security requirements. Teams with experience
in embedded security may also consider including protections against embedded
attacks including fault injection or side-channel analysis.

As you work through both designs, make sure you record all of your decisions in
what will become your design document.

.. admonition:: Tip

    The :term:`Crypto Interface` does not implement public key encryption or
    signing. We strongly recommend newer teams stick to only the features
    provided by it.


4. Start Simple and Develop Incrementally
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

With your simple design completed, your development team can begin
implementation. For many students, the eCTF will be the most complicated
development project that they have attempted. The temptation will be to try to
implement the entire design at once.

**Resist this urge.**

Teams will inevitably run into a number of unexpected roadblocks from bugs to
misinterpetations of rules to quirks of working on real hardware. If you go
straight from the :term:`Reference Design` and implement your full secure
design, so many things will have changed that it will be incredibly difficult to
find what has broken.

Instead, develop incrementally, making one small change and testing it. If it
fails or behaves unexpectedly, go back to your last working design and make sure
it is still working as a sanity check (if it fails, repeat going back until you
find a working state). This will make it easy to identify exactly where the
design broke. **Make sure you commit working versions to** :term:`Git` **so you
can always go back to where it was working**

For example, instead of implementing your full cryptographic protocol, you could
start small and iterate something like:

* Have a single board read a message from the host computer and echo it back
* Modify it to encrypt the message with a key of all 0s before echoing the
  :term:`Ciphertext`
* Modify it to allow it to use a key that is built into the binary at compile time
* Have the board send the encrypted message to another board, which echoes it
  to the host computer
* Have the second board decrypt the message before sending the :term:`Plaintext`
  to the host computer

Since there are only a couple changes between each step, if anything breaks, it
will be easy to track down where the problem was introduced.

If you need any help with development, reach out to the organizers
(``@organizers``), mentors (``@mentors``), and your fellow competitors in
``#tech-support`` on :doc:`../../about/slack`.

.. admonition:: Tip

  The more that has changed from the prior working design, the harder it will be
  to identify where your system broke, so take development step by step


Tips and Tricks
^^^^^^^^^^^^^^^

* Don't be afraid to ask for help! The competition Slack channel is an excellent
  resource where you can ask the organizers and your fellow competitors
  questions. If you're running into a problem, it's very likely that your fellow
  competitors have as well, so asking in ``#tech-support`` gives you the best
  shot at getting a good answer. You can also ask the organizers in your team
  channel if the question is sensitive to your design
* If you want to get the attention of the organizers, make sure to use
  ``@organizers`` in your message rather than tagging individual organizers



.. admonition:: Quote

    "I enjoyed having the opportunity to tackle a hard problem in a more
    challenging environment than I'm used to. It forced me to do a lot of
    research into issues that I had never really thought about before." - Former
    eCTF Competitor
