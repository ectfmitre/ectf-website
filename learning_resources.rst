Learning Resources
==================

Some learning resources to get familiar with the fundamentals of the competiton
are: 

**If you find useful resources, please reach out to the organizers so they can
add it to this list.**

CTFs
----
- UTSA Learning League, CTF Lane: https://learn.utsacybercomp.org/

Development
-----------
- Git tutorial: https://git-scm.com/docs/gittutorial
- Docker docs: https://docs.docker.com/manuals/

C Resources
-----------

- “The C Programming Language” by Kernighan and Ritchie
- https://www.learn-c.org/

Rust Resources
--------------

- The Rust Programming Language: https://doc.rust-lang.org/stable/book/
- The Embedded Rust Book: https://docs.rust-embedded.org/book/
- The Rustonomicon: https://doc.rust-lang.org/nomicon/

Embedded Resources
------------------

- https://embeddedartistry.com/beginners/ 
- https://embedded.fm/blog/ese101
- Linker scripts:
    - GNU: https://ftp.gnu.org/old-gnu/Manuals/ld-2.9.1/html_chapter/ld_3.html
    - OSDev Wiki: https://wiki.osdev.org/Linker_Scripts
    - Everything You Never Wanted To Know About Linker Script: https://mcyoung.xyz/2021/06/01/linker-script/

Cybersecurity Resources
-----------------------

- http://myemates.com/styled/cybersecurity.html
    - Recommended content:
        - Cybersecurity design principles & Cryptography
        - Encryption
        - Hashing
        - Buffer overflows

Embedded Security in the Wild
-----------------------------

- https://media.ccc.de/v/38c3-hacking-the-rp2350
