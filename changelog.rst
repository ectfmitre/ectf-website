Changelog
=========

January 15, 2025
----------------
* Release of 2025 challenge description

January 16, 2025
----------------
* Removed incorrect reference to "channel 5" on graphics in :doc:`2025/flags/attack_flags` (credit to UIUC)
* Added reference to :doc:`learning_resources`

January 17, 2025
----------------
* Removed outdated Docker packages in :doc:`2025/getting_started/machine_setup` (credit to CMU)
* Fixed outdated language in :ref:`2025_scenarios` (credit to Purdue)
* Reduced Decoder FPS requirement to 10FPS in :ref:`2025_throughput_requirements` (credit to OSU)

January 21, 2025
----------------
* Fixed title of archived :doc:`2024/getting_started/index`
* Fixed MSDK link in :doc:`2025/getting_started/openocd` and :doc:`2025/getting_started/max78000_fthr`
* Clarified :ref:`2025_sr3`
* Clarified Decoder :ref:`2025_timing_requirements`

January 24, 2025
----------------
* Added advice for :doc:`2025/flags/design_doc`

January 27, 2025
----------------
* Added Decoder ID as input to Generate Subscription in :ref:`2025_build_sat` and :ref:`2025_build_tool_calls`

January 28, 2025
----------------
* Added Decoder ID size to :ref:`2025_size_requirements`
* Specified global secrets path in :ref:`2025_build_decoder`

January 31, 2025
----------------
* Updated :doc:`2025/flags/debugger_flag`

February 3, 2025
----------------
* Clarified :ref:`2025_build_decoder` requirements
* Updated :doc:`2025/getting_started/boot_reference` with new flow

February 4, 2025
----------------
* Added :ref:`25_bci`

February 12, 2025
-----------------
* Fixed link in :ref:`25_bci`

February 18, 2025
-----------------
* Clarified subscription persistence requirements
* Moved Handoff workshop to Thursday, February 20

February 21, 2025
-----------------
* Updated Attack the Reference Design due date