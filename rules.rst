Rules
=====

In addition to the details of the competition described elsewhere on this site,
all teams must follow the following rules. Teams who violate these rules may be
penalized or even disqualified from the competition. Please contact the
organizers if you have any questions.

1. In addition to the rules provided by MITRE, participants must also adhere to
   all local laws and the policies and procedures stipulated by their local
   organization/university.
2. Every member of the team, including the advisor, must have filled out the
   Participant Agreement.
3. MITRE reserves the right to update, modify, or clarify the rules and
   requirements of the competition at any time, if deemed necessary by the eCTF
   organizers.
4. When submitting your secure design during :ref:`handoff`, all source code and
   documentation must be shared. Source code and documentation will be shared
   with attacking teams to discourage security-by-obscurity and to accelerate
   attack development and encourage more sophisticated techniques for both
   sides.
5. No part of your submission (documentation and source code) during
   :ref:`handoff` may be intentionally obfuscated, including using programming
   languages other than :ref:`2025_allowed_languages` without explicit approval by
   the eCTF organizers. Compile-time or run-time obfuscations do not fall under
   this and are explicitly allowed.
6. You may only attack the student-designed systems explicitly designated as
   targets :doc:`2025/flags/attack_flags`, and such attacks may only occur when
   you are in the :ref:`attack_phase` of the competition.

    1. Any MITRE provided tools or infrastructure is NOT in scope for attack.
    2. A secure MITRE :term:`bootloader <bootloader>` will be running on the
       physical device that securely loads attack-phase designs - this
       bootloader is NOT in scope for attack.
    3. If you have any question about whether a component is “in scope”, please
       reach out to the organizers for clarification before attacking.

7. All :doc:`2025/flags/attack_flags` captured must be validated by submitting a
   brief description of the attack. Attack descriptions should be sufficiently
   detailed to allow the defender to correct their vulnerability. eCTF
   organizers may remove points awarded for capturing flags that are not
   validated before the completion of the eCTF. See :doc:`about/scoreboard` for
   instructions on submitting descriptions
8. Flag sharing across teams is not permitted and will result in immediate
   disqualification.
9. Your design may not permanently lock or "brick" itself if it detects an
   attack. See No permanent lockouts are allowed. See the
   :ref:`2025_timing_requirements` for timing and performance requirements and
   allowed delays.
10. Your system must work with the unmodified platform and hardware that was
    provided. Switching to a different platform during the :ref:`design_phase`
    is not allowed. You may modify your hardware to aid in attack development
    and exploitation, but your design will be tested with unmodified hardware
    during :ref:`handoff`.
11. All documents that are submitted (e.g., design document and write-ups) must
    be provided in PDF format.
12. Use of AI-assisted code generation is allowed, however secure coding
    practices require that you actually have a full understanding of what has
    been written, so we recommend against reliance on AI-generated code.
13. All design, code, and documentation submitted must be created by registered
    team members. The
    following exceptions are allowed: code from the reference design,
    properly-attributed open source code and libraries, code shared by
    organizers or other teams in public channels on the Slack workspace, and
    trivial code snippets with no substantial impact to your project (e.g.,
    using Stack Overflow to understand how to write a for loop,
    using code examples for calling into an open source library).
14. While the Handoff process is designed to catch as many functional
    requirement and rule violations as possible, due to the complexity of the
    designs, situations may arise where teams make it into the Attack Phase with
    designs that are not fully compliant. In this case, the organizers reserve
    the right to determine the proper resolution. Violations deemed to be minor
    and accidental may be allowed to continue in the Attack Phase with no
    penalty. Violations deemed to be intentionally designed to skirt the rules
    and/or to obfuscate the violation to avoid detection may be penalized up to
    and including expulsion from the competition. Violations deemed to be in
    between may require the team to fix the violation before continuing in the
    Attack Phase, move back to the Design Phase, or other suitable resolutions.
