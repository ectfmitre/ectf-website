About the eCTF
==============

The Embedded Capture the Flag (eCTF) is an embedded security competition run
over the spring semester by `MITRE <https://mitre.org/>`_ that puts participants
through the experience of trying to create a secure system and then learning
from their mistakes.

The eCTF is unique from other CTF competitions that you may be familiar with.
First the focus of the eCTF is on **embedded systems**, which present a new set
of challenges and security implications. Second, the eCTF balances **defense and
offense** by testing and rewarding both sets of skills during the Design Phase
and Attack Phase, respectively. Finally, to allow the complexity of the eCTF,
the competition runs **over the entire spring semester** from mid-January
through mid-April.

.. image:: ../_static/eCTF\ Phases.png

.. _design_phase:

Design Phase
------------
In the Design Phase, each team must design and implement a system that meets a
set of **functional requirements** and **security requirements**. Teams will be
provided with a :term:`Reference Design` that meets the functional requirements
but intentionally *does not attempt to meet any security requirement*. Teams may
use the reference design as a starting point or build their design from scratch.

During the Design Phase, teams may score points by capturing
:term:`Design Flags` which show that teams are making progress
towards a complete design. Flags must be submitted on the :doc:`scoreboard` by
their deadlines for points to be awarded. Additionally, teams may score points
by finding functional errors in the tools and example design provided by the
organizers as part of the :term:`Bug Bounty`.

.. _handoff:

Handoff
-------
Once a team has finished their design, they may submit it to Handoff. The
process of a team submitting their design to the eCTF organizers at the end of
the Design Phase. Only designs that pass all functional testing and meet the
rules of the competition will complete Handoff and move to the Attack Phase.

.. _attack_phase:

Attack Phase
------------
During the Attack Phase each design that has been validated during Handoff will
be made available to other teams for attack. Teams will be able to load other
teams' designs on a physical microcontroller with a secure :term:`bootloader`
provided by the organizers.

.. _award_ceremony:

Award Ceremony
--------------
After the competition has ended, teams will gather in person and virtually at
the Award Ceremony. The Award Ceremony is an opportunity to get to meet the eCTF
organizers, the sponsors, and your fellow competitors and share about your
experiences during the competition.

Information about the 2025 Award Ceremony can be found at
:doc:`../2025/schedule/award_ceremony`.

.. toctree::
    :maxdepth: 1
    :caption: Sections:

    scoreboard
    scoring_system
    slack
