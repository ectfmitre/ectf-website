Scoreboard
==========

The scoreboard for the eCTF allows teams to submit flags and keeps track of the
scores of each team. The scoreboard can be found at https://ectf.ctfd.io/scoreboard

.. admonition:: Tip

    Check your team's :doc:`slack` channel for your team's scoreboard login
    information credentials. All members of your team will use the same
    credentials

Using the Scoreboard
--------------------
Each team will be provided a **single** login for the scoreboard by the eCTF
organizers that all team members must share. To submit a flag, go to the
`Challenges page <https://ectf.ctfd.io/challenges>`_ and click on
one of the open challenges. On the subsequent popup, enter the flag in the text
box and submit.

If the flag is rejected, first ensure that it has been formatted and entered
correctly (see :term:`flags`). If it is still rejected, do not continue to
reenter it and talk to the organizers, as the scoreboard will begin rate
limiting your submission attempts.

Once you have submitted an :ref:`Attack Phase Flag<attack_phase_points>`, you
will be prompted to submit an explanation of your flag. You must submit a brief
writeup of how you captured that flag with sufficient detail for a reader to
understand the gist of how the flag was captured.