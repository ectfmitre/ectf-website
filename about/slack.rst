Slack
=====

Once the competition kicks off, the vast majority of communication happens on an
eCTF Slack workspace. As well as being a space to talk to your fellow
competitors, MITRE organizers, and competition sponsors, Slack has a number of
automation features to support the competition including the
:term:`Testing Service` and parts of :term:`handoff`.

Channel Structure
-----------------

The following channels will be available for use by teams:

* ``#general``: General eCTF discussion
* ``#announcements``: Read-only feed of competition announcements
* ``#tech-support``: Place to ask for help with technical problems
* ``#2025-tips``: Read-only feed of helpful tips
* ``#2025-attack``: Channel for teams in the Attack Phase. Teams will be added
  to this channel once they pass :ref:`handoff`.
* ``#2025-attack-packages``: Read-only feed of available Handoff Packages in the
  Attack Phase. Teams will be added to this channel once they pass
  :ref:`handoff`.
* ``#random``: Miscellaneous posts
* ``2025-TEAMID``: A private channel only accessible by your team and the eCTF
  organizers. The organizers or your advisor will add you to this channel.

Slack Tips
----------

* Unless your question is sensitive to the details of your design, use
  ``#tech-support`` as much as possible, as other competitors may know the
  answers to your questions and your question may help others who are struggling
  with the same problem
* Use ``@organizers`` on new messages where you want to get the attention of the
  MITRE organizers. This is almost always better than tagging individual
  organizers, as other organizers may be able to assist you. The organizers can help with
  rules clarifications and technical
* Use ``@mentors`` on new messages where you want to get the attention of the
  competition mentors. The mentors are industry experts in cybersecurity and
  embedded security with decades of experience, so use them to help you better
  understand the competition and the problems and potential solutions you face.
