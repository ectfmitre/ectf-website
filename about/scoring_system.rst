Scoring System
==============

To support the unique style of the eCTF, MITRE has developed a unique scoring
system to match. The eCTF incorporates a number of methods to reward teams for
progressing through and excelling in the competition.

Teams may earn points through three different types of points: Design Phase
Points, Attack Phase Points, and Miscellaneous Points.

Design Phase Points
-------------------
During the :ref:`design_phase`, teams may earn points by making progress towards
submitting their design.

First, points can be earned through capturing :doc:`../2025/flags/design_flags`.
A number of challenges have been laid out to allow teams to demonstrate that
they are staying on track with their secure design. By completing these
challenges, teams will uncover :term:`flags` that can be entered on the
:doc:`scoreboard`. Teams will earn full points for submitting the flag on time
and partial points for submitting after the deadline.

Second, points can be earned by participating in the :ref:`2025_bug_bounty`. If your
team happens to find a bug in the :term:`reference design`, you can earn points
for both identifying the bug and submitting a fix.

.. _attack_phase_points:

Attack Phase Points
-------------------
The majority of points earned during the competition are done so during the
:ref:`attack_phase`. Points can be earned both for your design successfully
withstanding the attacks of other teams (:term:`Defensive Points`) and for
successfully attacking other teams (:term:`Offensive Points`).

.. admonition:: Tip

    Most points in the eCTF are earned during the Attack Phase, so make sure
    you stay on track during the Design Phase to make it past Handoff on time

Defensive Points begin passively accruing the second a team passes
:ref:`handoff` and enters the Attack Phase. For each
:doc:`Attack Phase Flag <../2025/flags/attack_flags>` of a team that has not
been captured by another team, the defending team earns a fixed number of points
per hour. Once another team captures a flag, the defending team retains the
points that had been earned by that flag, but it no longer continues to accrue
further points.

Offensive Points are actively earned by capturing the Attack Phase Flags of
other teams. Attack Phase Flags are the most complex component of the eCTF
scoring system, as they have the most complex requirements. Teams are rewarded
not only for the number of flags that they are able to capture, but also the
difficulty and speed of the capture.

The difficulty of any particular flag is not readily apparent before the
competition begins and it is neither fair nor feasible to manually judge the
flags in real time or after the competition has ended. Instead, the eCTF scoring
system uses two proxies to measure difficulty: speed of capture and number of
teams capturing. The faster a flag is captured after it becomes available and
the more teams capture that flag, the easier it is likely is to capture it.

Similar to Defensive Points, the number of points available for an Attack Phase
Flag accrues from the time the team enters the Attack Phase until the second it
is first captured, at which point it no longer becomes more valuable.

These points are initially all granted to the first team to capture the flag.
However, as more teams capture the flag, that initial point value gets split
between the teams that capture it. This results in the often confusing
phenomenon of teams' scores *decreasing* as a flag they captured is captured by
other teams, reducing their share of the flag.

.. warning::

    Your score in the attack phase can actually go *down* when another team
    captures a flag you have already captured

To reward teams for their speed in capturing a flag, the points of a flag
capture are not split evenly, but rather based on the number of shares they earn
from how long it took a team to capture the flag after the flag's first capture.
The first team to capture a flag earns the maximum number of shares. Afterwards,
the number of shares a team would earn decreases for each hour that passes after
the first capture.

Additionally, to discourage teams hoarding flags, bonus points are awarded to
the first team to capture any given flag.


Miscellaneous Points
--------------------
Finally, there are a few additional ways to earn points.

First, teams who submit a design to Handoff may earn Documentation Points. Teams
can earn points on a sliding scale based on the clarity of their code and
supplemental documentation. These points are revealed at the
:ref:`award_ceremony`.

Second, after the :doc:`scoreboard` closes, teams will be invited to submit a
poster describing their design and experiences in the competition. These posters
will be presented during the :term:`Poster Session`. Posters will be awarded on
a sliding scale based on feedback from a panel of expert judges attending the
Poster Session.

Finally, the eCTF organizers may announce additional opportunities for earning
points throughout the competition including participation in workshops or
other activities.

