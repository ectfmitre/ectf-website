Design Phase Flags
==================

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

During the :ref:`design_phase`, teams can show that they are staying
on tracks of time and earn some points by submitting Design Phase Flags.

**New in 2024:** Earn full points by submitting by the deadline. Flags submitted
after the deadline will earn half points.


.. list-table:: Design Phase Flags
    :header-rows: 1

    * - Milestone
      - Flag Format
      - Due Date
      - Points
      - Description
    * - Read Rules
      - ``ectf{readtherules_*}``
      - January 24
      - 100
      - If you read **all** the rules, you'll know
    * - Boot Reference Design
      - ``ectf{bootreference_*}``
      - January 26
      - 100
      - Provision and boot the :doc:`../components/reference_design` to receive
        a flag
    * - Design Document
      - ``ectf{designdoc_*}``
      - January 31 
      - 100
      - Submit an initial design document containing high-level descriptions of
        how each host tool and system function will work. It should be clear
        from your descriptions how your system meets the
        :doc:`../specs/functional_reqs` and :doc:`../specs/security_reqs`. Your
        design **may** change after submitting this draft, but we recommend
        updating this as your design changes and including it in your final
        submission for documentation points.
    * - Debugger
      - ``ectf{debugger_*}``
      - February 7 
      - 100
      - Show that you can use a debugger. More information about capturing this
        flag will be released soon.
    * - Testing Server
      - ``ectf{testing_*}``
      - February 14 
      - 100
      - Use the testing server and read the results.
    * - Final Design Submission
      - ``ectf{attackphase_*}``
      - 
      - 1,000
      - Pass :doc:`../events/handoff` to earn points and enter the
        :ref:`attack_phase`
    * - Bug Bounty
      - 
      - 
      - Up to 200
      - See :ref:`2024_bug_bounty` below

.. _2024_bug_bounty:

Bug Bounty
----------

If your team happens to find a bug in the reference design, you can earn points
for it! Your team will receive 100 points for each bug found, and another 100
points if you submit a corresponding fix. If multiple teams find the same bug,
points will be distributed on a first come, first serve basis.

Sometimes whether an issue is truly a bug (or a feature!) is a matter of opinion
- the eCTF organizers reserve the right to reject bug reports for trivial issues
and combine multiple similar reported bugs into one. Submitted bugs will be
accepted if there is a violation of the functional requirements in the reference
design that prevents it from working correctly.

Submissions for typos or clarifications on documentation provided by the
organizers will not be considered for additional points, although we appreciate
being notified about these mistakes so we can make the appropriate edits and
provide further explanation where it is necessary.

Please submit Bug Bounty requests through your private team channel on
:doc:`../../../about/slack` tagging ``@organizers``.