Post-Quantum Cryptography Side-Channel Analysis Challenge
---------------------------------------------------------

Post-quantum cryptography aims to provide secure algorithms that would resist
the attacks of an adversary with access to a sufficiently powerful quantum
computer. The final algorithms are in the last stages of being selected, and
while they are believed to be cryptographically strong, there is a growing field
of research demonstrating that they can be weak to side-channel analysis
attacks.

This year, teams will be able to earn bonus points to attempt some of these
attacks. Teams will earn 250 points for conducting successful side-channel
analysis attacks to extract the private key from two sets of power traces of
hardware performing ML-KEM key decapsulation.

If you successfully extract either of the two keys, please post them to your
team channel tagging the organizers.

See the announcements channel on Slack for a video going over the challenge as
well as the two sets of traces. Unfortunately, only the first two challenges
mentioned in the talk (attacking the pre-recorded traces) are eligible for
points.

Thank you to Dr. Alexander Nelson and his team at the University of Arkansas for
their help in pulling the challenge together