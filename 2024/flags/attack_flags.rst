Attack Phase Flags and Scenarios
================================

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

.. admonition:: Tip

  **Understanding the contents of this page is critical to properly securing your
  design**. If something is unclear, please reach out to the organizers on
  :doc:`../../../about/slack` to clarify.


.. _2024_scenarios:

Attack Phase Scenarios
----------------------
Attackers will have access to a number of different combinations of
:ref:`Application Processors<2024_application_processor>`, :ref:`2024_components`, and
other information. Each setup represents a realistic scenario that willl test a
design's success at meeting one or more :doc:`../specs/security_reqs` by
embedding :ref:`2024_attack_flags` into different parts of the setup. 

The Attack Phase scenarios use multiple :ref:`deployments<2024_build_deployment>`
between scenarios and flags to ensure information an attacker validly has in one
scenario doesn't undermine another. The table below summarizes the flags,
scenarios, relevant security requirements, and deployments.

.. list-table:: Flag Scenario Mapping 
    :header-rows: 1

    * - Flag
      - :ref:`Scenario<2024_scenarios>`
      - :doc:`Security Requirement<../specs/security_reqs>`
      - :ref:`Deployment<2024_build_deployment>`
    * - Operational Pin Extract
      - :ref:`AP1<2024_ap1>`
      - :ref:`SR3<2024_sr3>`
      - D1
    * - Operational Pump Swap
      - :ref:`AP1<2024_ap1>`
      - :ref:`SR5<2024_sr5>`
      - D1
    * - Damaged Boot
      - :ref:`AP2<2024_ap2>`
      - :ref:`SR1<2024_sr1>`
      - D1
    * - Supply Chain Boot
      - :ref:`AP3<2024_ap3>`
      - :ref:`SR1<2024_sr1>`
      - D3
    * - Supply Chain Extract
      - :ref:`AP3<2024_ap3>`
      - :ref:`SR4<2024_sr4>`
      - D3
    * - Black Box Boot
      - :ref:`AP4<2024_ap4>`
      - :ref:`SR2<2024_sr2>`
      - D2
    * - Black Box Extract
      - :ref:`AP4<2024_ap4>`
      - :ref:`SR4<2024_sr4>`
      - D2



.. _2024_ap1:

Attack Phase 1: Operational Device
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


The attacking team is provided with: 

* AP provisioned for Component A & Component B
* Component A
* Component B

The attacker has access to a fully working medical system but **does not have
access to the PIN, or Replacement Token**. This scenario represents an attacker
in physical proximity of a provisioned medical device. This scenario is
provisioned from deployment 1.

.. _2024_ap2:

Attack Phase 2: Damaged Device
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The attacking team is provided with:

* AP provisioned for Component C & Component D
* Component C

The attacker has access to a system, but one component has been damaged and
removed. In this scenario, an attacker could have gained access to a device
while it was being serviced. This scenario is provisioned from deployment 1.

.. _2024_ap3:

Attack Phase 3: Supply Chain Poisoning  
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The attacking team is provided with:

* AP provisioned for Component A & Component B
* Component A
* No physical access to system

Replace Token and Attestation Pin are used by the technician repairing the
system:

The attacker has managed to infiltrate the supply chain of medical device
components and sell counterfeit replacement components in hopes that they are
installed onto medical devices. A medical device with a damaged component has
been taking to a facility for repair, but the counterfeit component is replaced
instead of a valid component. The attacker cannot receive the result of the
repair, but can exfiltrate data through a C2 link build into the counterfeit.
This scenario is provisioned from deployment 3. 

.. _2024_ap4:

Attack Phase 4: Black Box
^^^^^^^^^^^^^^^^^^^^^^^^^

The attacking team is provided with:

* Component X

The attacker has access to a legitimate individual component that was received
by an insider at the component manufacturer. The attacker is attempting to
reverse-engineer the Component's functionality by booting the component and/or
extracting sensitive data. This scenario is provisioned from deployment 2.

Flags are available from completing functional tasks on other teams designs
during the attack phase of the competition. Each flag tests a specific
:doc:`../specs/security_reqs`.

.. _2024_attack_flags:

Attack Phase Flags
------------------
During the Attack Phase, teams will test the security of other teams' designs by
attempting to capture Attack Phase :term:`Flags<flags>`. Each flag represents proof on
an attacker's ability to compromise one or more security requirements of a
design.


.. list-table:: 
    :header-rows: 1

    * - Flag
      - Format
      - Description
    * - Operational Pin Extract
      - ``ectf{pinextract_*}``
      - Return confidential attestation data from the operational device
    * - Operational Pump Swap
      - ``ectf{pumpswap_*}``
      - Your medical components have been installed in an insulin pump. Attack
        the actuator in an insulin pump post-boot to the incorrect state.
    * - Damaged Boot
      - ``ectf{damagedboot_*}``
      - Cause a damaged board missing a component to boot
    * - Supply Chain Boot
      - ``ectf{supplychainboot_*}``
      - Cause a board missing a component, with a known PIN, and no physical
        access to boot
    * - Supply Chain Extract
      - ``ectf{supplychainextract_*}``
      - Extract confidential data from a valid component connected to an AP with
        a known PIN and no physical access
    * - Black Box Boot
      - ``ectf{blackboxboot_*}``
      - Cause a black box component to boot
    * - Black Box Extract
      - ``ectf{blackboxextract_*}``
      - Extract confidential data from a black box component
