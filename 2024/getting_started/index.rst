2024 eCTF Getting Started
=========================

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

This section of the docs will help with getting your machine set up to compete
in the 2025 eCTF! The purpose of this competition is to help develop your skills
in software, hardware, embedded systems, and security. Don't worry if some of
these are new to you; the point is to learn!

.. admonition:: Tip

    We recommend starting with reading :doc:`ectf_guide` to
    help your team get started off on the right foot.

The tooling for the 2025 eCTF has made significant changes from previous years!
These changes aim to make the build process more transparent to competitors
during the design portion of the competition.

Your team is resopnsible for developing the firmware to run on the MAX78000FTHR
development board. To allow for standardization of builds, firmware compilation
will be done utilizing :doc:`nix`, a tool that creates reproducible build
environments.  Example code for this competition, as well as examples provided
by Analog Devices will be provided in C. Your team is welcome to utilize other
pre-approved languages (see :ref:`2024_allowed_languages`). **Any languages outside
of these will require approval from the organizers.**

Information on getting your computer set up with Nix and other tools is provided
in the sections below.

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    ectf_guide
    machine_setup
    max78000_fthr
    ectf_breakout
    nix
    poetry
    openocd
    learning_resources

The section below is **MacOS** only:

.. toctree::
    :maxdepth: 1

    daplink
