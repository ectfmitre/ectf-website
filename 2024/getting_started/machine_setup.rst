Machine Setup
=============

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

To set up your machine for the eCTF, we require that you install the following list
of software:

- Git
- :doc:`nix`

This year, Nix will handle the installation of additional required dependencies.

- :doc:`openocd`
- arm-none-eabi-gcc
- make
- Python3.9
- :doc:`poetry`

Nix
---

Nix supports installation natively on Linux and MacOS. Nix installation on Windows
can be achieved through the Windows Subsystem for Linux (WSL). Alternatively, 
virtualization software such as VirtualBox or VMWarePlayer can be used to install 
a Linux distribution for development alongside a Windows system.

Nix can be installed at:
https://nixos.org/download

Specifics on how to use Nix, and how to install the remaining dependencies using Nix
can be found at :doc:`nix`.

Git
---

Git is an open-source version control system. It will allow your team to collaborate on a single code base while managing a history of all the changes you make.
Git is required to submit your design for testing and progression to the Attack Phase. This makes it easy for the organizers to download your code for testing and allows your team to tag a specific version of your code you want to submit.

All Platforms:

- https://git-scm.com/downloads

