Poetry
======

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

Poetry is a Python packaging and dependency management utility. Poetry is utilized 
in the 2024 eCTF to manage dependencies for the eCTF tools, utilities, and additional 
build infrastructure.

Poetry should be automatically installed in your Nix environment. To set up Nix,
see :doc:`nix`. If you want to explore Poetry without Nix, it can be easily installed 
with Pip ``pip install poetry``. Documentation on Poetry can be found at: https://python-poetry.org/.

Poetry utilizes two files to handle dependency management. These files are:

- pyproject.toml
- poetry.lock

The dependencies and project information are stored in `pyproject.toml`, and 
poetry.lock ensures that dependencies are all of the correct revision.

The eCTF example design will come with a Poetry project. This project can 
be installed with ``poetry install``.

To use poetry outside of a pre-existing project, utilize ``poetry init`` to create 
a new project.

Once poetry is initialized, you can launch into an interactive shell with ``poetry shell``. 
Additionally, you can launch programs inside of the environment with ``poetry run {command}``.

Additional dependencies can be installed with ``poetry add {dependency}`` and removed with
``poetry remove {dependency}``. 
