Learning Resources
==================

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

Some learning resources to get familiar with the fundamentals of the competiton
are: 

**If you find useful resources, please reach out to the organizers so they can
add it to this list.**

Development
-----------
- Git tutorial: https://git-scm.com/docs/gittutorial

C Resources
-----------

- “The C Programming Language” by Kernighan and Ritchie
- https://www.learn-c.org/

Embedded Resources
------------------

- https://embeddedartistry.com/beginners/ 
- https://embedded.fm/blog/ese101

Cybersecurity Resources
-----------------------

- http://myemates.com/styled/cybersecurity.html
    - Recommended content:
        - Cybersecurity design principles & Cryptography
        - Encryption
        - Hashing
        - Buffer overflows
