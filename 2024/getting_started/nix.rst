Nix
===

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

Nix is a system that can be utilized to create reproducible build systems. If you have already 
installed Nix, as shown in the :doc:`machine_setup`. You can begin to learn how to utilize Nix 
to standardize your build system for the 2024 eCTF.

Before the release of the example design, following `shell.nix` file can be utilized to build 
firmware for the MAX78000FTHR and explore the platforms capabilities.

Save the following file to `shell.nix` in the directory that you intend to use.

.. code-block:: nix
    :caption: shell.nix

    { pkgs ? import <nixpkgs> {}
      , fetchgit ? pkgs.fetchgit
    }:

    pkgs.mkShell {
      buildInputs = [
        pkgs.gnumake
        pkgs.python39
        pkgs.gcc-arm-embedded
        pkgs.poetry
        pkgs.cacert
        #(pkgs.callPackage analog_openocd.nix { })
        pkgs.minicom
      ];

      msdk = builtins.fetchGit {
        url = "https://github.com/Analog-Devices-MSDK/msdk.git";
        ref = "refs/tags/v2023_06";
      };
      shellHook =
        ''
          cp -r $msdk $PWD/msdk
          chmod -R u+rwX,go+rX,go-w $PWD/msdk
          export MAXIM_PATH=$PWD/msdk
        '';
    }

This nix environment will install the dependencies listed in the `buildInputs` section. 
Additional packages can be found at https://search.nixos.org/packages. The dependencies installed 
here are: gnumake, python39, gcc-arm-embedded, poetry, cacert, and minicom. The line that is 
commented out for analog_openocd.nix is an example of how to create custom nix packages if 
software that you intend to use is not available from the nix repository.

To enter into a development environment and install the nix packages listed, utilize the 
`nix-shell` command. 

A specific fork of OpenOCD is needed to work with support for the MAX78000FTHR development board.
A custom Nix package, `analog_openocd.nix` is provided that will compile this iteration of OpenOCD 
from source. This operation is time-consuming, and as such has been commented out unless needed. 
Once Nix builds this tool once, it will not need to be re-build on each operation. To build OpenOCD,
save the file below to `analog_openocd.nix` in the same directory as `shell.nix` and uncomment the related line in `shell.nix`. 

.. code-block:: nix
    :caption: openocd_analog.nix

    nix
    { stdenv
    , lib
    , pkg-config
    , hidapi
    , jimtcl
    , libjaylink
    , libusb1
    , libgpiod
    , gcc
    , gnumake
    , coreutils
    , autoconf
    , automake
    , texinfo
    , git
    , libtool
    , which
    , libftdi1
    }:

    stdenv.mkDerivation {
      pname = "openocd-analog";
      version = "0.12.0";

      src = builtins.fetchGit {
        url = "https://github.com/analogdevicesinc/openocd.git";
        ref = "release";
        submodules = true;
      };

      nativeBuiltInputs = [ pkg-config ];

      buildInputs = [
        hidapi
        gcc
        gnumake
        coreutils
        pkg-config
        autoconf
        automake
        texinfo
        git
        jimtcl
        libusb1
        libjaylink
        libftdi1
        libtool
        which
      ];

      postPatch = ''
        substituteInPlace src/jtag/drivers/libjaylink/autogen.sh --replace "LIBTOOLIZE=glibtoolize" "LIBTOOLIZE=libtoolize"
      '';

      enableParallelBuilding = true;

      configurePhase = ''
        SKIP_SUBMODULE=1 ./bootstrap
        ./configure --prefix=$out --disable-werror
      '';

       meta = with lib; {
        description = "OpenOCD fork for Analog Devices microcontrollers";
        longDescription = ''
          This is a fork of OpenOCD by ADI,
          which brings support to MAXIM MCUs microcontroller.
        '';
        homepage = "https://github.com/analogdevicesinc/openocd.git";
        license = licenses.gpl2Plus;
        maintainers = with maintainers; [ eCTF ];
      };
    }

After completion of the `nix-shell` command, you will be put into a shell in your nix environment. 
To see the magic of nix, type `which make` and see that `make` has been installed specific to your 
nix environment!

