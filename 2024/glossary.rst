2024 Glossary
=============

.. note::

    If there are any terms that you had to learn in the eCTF that are missing
    from this list, please let the organizers know so we can add it

.. glossary::
    AP
        See :ref:`application_processor`

        The Application Processor (AP) is the "brains" of the :term:`misc`,
        responsible for communicating with the :term:`Host Computer` and
        interacting with the :term:`Components`

    Attestation PIN
        See :ref:`attest`

        The Attestation PIN is a confidential PIN that is used to authenticate
        a user's privilege to run the Attest functionality.

    Components
        See :ref:`components`

        The Components represent the many additional chips that may be found
        on the :term:`misc` that interface with the :term:`AP`.

    Host Computer
        See :ref:`host_computer`.

        The Host Computer is a general-purpose computer used to communicate with
        the :term:`MISC`.

    MISC
        See :ref:`medical_device`

        The Medical Infrastructure Supply Chain (MISC) is the name of the system
        your team is building.
    
    Replacement Token
        See :ref:`replace`

        The Replacement Token is a confidential piece of data that is used to
        authenticate a user's privilege to run the Replace functionality.