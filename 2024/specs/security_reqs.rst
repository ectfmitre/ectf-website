Security Requirements
=====================

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

This section defines the security requirements of your design. These properties
will not be tested or evaluated during :doc:`../events/handoff`. Instead,
other teams will earn points for identifying and exploiting failures to properly
meet these requirements by capturing
:doc:`Attack Phase Flags<../flags/attack_flags>` during the
:ref:`attack_phase`. Use these requirements to inform your design
process, identifying and protecting critical data and code paths.

.. warning::
	Your design is **NOT** tested for its adherence to Security Requirements
	during :doc:`../events/handoff`.

.. _2024_sr1:

Security Requirement 1
----------------------
The :ref:`2024_application_processor` (AP) should only boot if all expected
:ref:`2024_components` are present and valid.

If the Medical Device is not in a fully functional state, the AP should not turn
on. If the AP is able to turn on in an invalid state, patient health and data
could be at risk. The Medical Device's AP should confirm the device's integrity
before booting. 

.. _2024_sr2:

Security Requirement 2
----------------------
Components should only boot after commanded to by valid AP has confirmed
integrity.

Any Component present on a Medical Device should only boot if it is part of a
valid system. If a Component is able to be used outside of a valid device --
including on counterfeit devices -- the safety of patients and the reputation of
the company could be at risk.  Sensitive data on the Components may also be at
risk to being leaked if the Component is able to boot. As the AP oversees
validating the device integrity, the Components should not boot until told to do
so by a valid AP. 

.. _2024_sr3:

Security Requirement 3
----------------------
The PIN and Replacement Token should be kept confidential.

PINs and Tokens play an integral role in segmenting access to privileged
operations in Medical Devices. Any number of actors could gain access to a
functioning device, so it is crucial that they are not able to extract these
secrets from it. If someone were able to, they would be able to gain access to
sensitive medical data and to introduce counterfeit parts into otherwise
legitimate systems.

.. _2024_sr4:

Security Requirement 4
----------------------
Component Attestation Data should be kept confidential.

The attestation data on each Component is essential to determining the validity
of the Component. If an attacker is able to access this data without the
required privilege, they may be able to recreate or modify the critical
Component. A leak of sensitive propriatary information would damage your
companies reputation and could potentially lead to counterfeit Components
risking patient safety.

.. _2024_sr5:

Security Requirement 5
----------------------
The integrity and authenticity of messages sent and received using the post-boot
MISC secure communications functionality should be ensured.

The ICs that your manufacturer creates will end up serving critical roles in
Medical Devices. Once the system is active, the communications between the
components must be secure from tampering, duplication, or forgery. If not,
patient safety could be at risk to malicious attacks or incidental data
corruptions.

.. note::
	:ref:`2024_sr5` does not require the communications to provide confidentiality,
	only integrity and authenticity.
