Detailed Specifications
=======================

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

Application Processor Firmware
------------------------------

There are four pre-boot required functional elements to the application
processor firmware. These elements directly relate to the host tools:

- List Components
- Boot Board
- Replace Component
- Attestation

All pre-boot functional requirements must align with the
:ref:`2024_timing_requirements`

Post-boot of the application processor, the required functionality for these
four functionality requirements no longer needs to be active. At this point, the
functionality in the ``secure_send`` and ``secure_receive`` functions must be
active.


.. _2024_misc_interface:

MISC Interface
^^^^^^^^^^^^^^

All messages sent between the AP and the :ref:`2024_host_computer` need to conform to
the format specified by the host tools. The ``host_messaging`` library included
in the example design provides an easy interface to send messages to the host
tools.

The host tools accept five types of messages:

.. list-table:: Host Messages
    :header-rows: 1

    * - Level
      - Format
      - Use
    * - Error
      - ``%error: {message}%``
      - Notify of an error/failure. Exits Host Tool
    * - Success
      - ``%success: {message}%``
      - Notify of a successful completion of a function. Exits Host Tool
    * - Info
      - ``%info: {message}%``
      - Provide functionality-relevant information
    * - Debug
      - ``%debug: {message}%``
      - Provide debug information (ignored by testing framework)
    * - Ack
      - ``%ack%``
      - Acknowledge the receipt of a message, requesting more data

List Components
^^^^^^^^^^^^^^^
The list components functionality must print *ALL* components that are connected
to the system as well as all components that the system was provisioned for.

Provisioned and found messages can be repeated for each relevant component ID.

.. list-table:: List output format
    :header-rows: 1

    * - Level
      - Message format
      - Example
    * - Info
      - ``P>{Provisioned Component ID prefixed by 0x}\n``
      - ``P>0x02\n``
    * - Info
      - ``F>{Found Component ID prefixed by 0x}\n``
      - ``F>0x02\n``
    * - Success
      - ``List\n``
      - ``List\n``
    * - Error
      - Any error message
      - ``Internal error\n``

Boot Board
^^^^^^^^^^

The boot board functionality must handle booting all provisioned components and
the AP. The boot message from each component and the AP should be sent in
separate messages.

After printing boot messages, the AP and Components should boot their respective
main functionality.

.. list-table:: Boot output format
    :header-rows: 1

    * - Level
      - Message format
      - Example
    * - Info
      - ``{Component ID prefixed by 0x}>{Component boot message}\n``
      - ``0x02>Hello world from a component\n``
    * - Info
      - ``AP>{AP boot message}\n``
      - ``AP>Hello world from the AP\n``
    * - Success
      - ``Boot\n``
      - ``Boot\n``
    * - Error
      - Any error message
      - ``Boot failed\n``

Replace Component 
^^^^^^^^^^^^^^^^^

The replace component function must handle replacing a provisioned component
with a new component. This function can make use of the replacement token for
validation (see :ref:`2024_size_requirements`). **The replace component function must
make changes constant across power cycles.**

.. list-table:: Replace output format
    :header-rows: 1

    * - Level
      - Message format
      - Example
    * - Success
      - ``Replace\n``
      - ``Replace\n``
    * - Error
      - Any error message
      - ``Replace failed\n``

Attestation
^^^^^^^^^^^

The return attestation function must print all fields of the component's
attestation data. This function can make use of the Attestation PIN (see
:ref:`2024_size_requirements`)


.. list-table:: Attestation format
    :header-rows: 1

    * - Level
      - Message format
      - Example
    * - Info
      - ``C>{Component ID prefixed by 0x}\n``
      - ``C>0x02``
    * - Info
      - ``LOC>{Attestation location}\n``
      - ``LOC>Boston\n``
    * - Info
      - ``DATE>{Attestation date}\n``
      - ``DATE>01/01/1970\n``
    * - Info
      - ``CUST>{Attestation customer}\n``
      - ``CUST>MITRE\n``
    * - Success
      - ``Attest\n``
      - ``Attest\n``
    * - Error
      - Any error message
      - ``Attest failed\n``

Component Firmware
------------------

The Component firmware can communicate with the AP through all of the pre-boot
required functional elements except for **Replace Component**.

The component attestation data is stored on each component as part of the build
process. Attestation data storage and transfer must align with the
:ref:`2024_size_requirements`

Post-boot of a component, the required functionality for validating a component
no longer needs to be available. At this point, the functionality in the
``secure_send`` and ``secure_receive`` functions must be active.

.. _2024_timing_requirements:

Post Boot Functionality
-----------------------

The Components that you design end up in a fully provisioned and working medical
device. In order to protect the operation of this medical device, generic
security functions must be implemented to allow for secure communications. Any
post-boot functionality implemented by the organizers will utilize these
functions to change the functionality of the overall medical system. |flag|

These functions are prototyped in the :doc:`../components/reference_design` and
must be implemented and must keep the same functional interfaces.

- ``secure_send``
- ``secure_receive``
- ``get_provisioned_ids``
  - **AP ONLY**

Timing Requirements
-------------------

.. list-table:: Timing Requirements
    :header-rows: 1

    * - Operation
      - Maximum Time for Completion
    * - Device Wake
      - 1 second
    * - Attestation
      - 3 seconds
    * - List Components
      - 3 seconds
    * - Boot
      - 3 seconds
    * - Replace Component
      - 5 seconds


.. _2024_size_requirements:

Firmware Size Requirements
--------------------------

.. list-table:: Firmware Size Requirements
    :header-rows: 1

    * - Component
      - Size
    * - Component ID
      - 4 bytes (Last byte is valid I2C address)
    * - Replacement Token
      - 16 bytes
    * - Attestation PIN
      - 6 bytes
    * - AP Boot Message
      - Max 64 bytes
    * - Attestation Customer Name
      - Max 64 bytes
    * - Attestation Manufacture Location
      - Max 64 bytes
    * - Attestation Date
      - Max 64 bytes
    * - Component Boot Message
      - Max 64 bytes

.. |flag| image:: ../../_static/Rules_Read.png
    :height: 2 ex

Host Tools
----------

Unlike previous years, the host tools this year are created by the organizers
and read-only. The host tools create a basic input/output format for
communications between your devices and the host computer. More information on
the host tools is available :doc:`here <../components/host_tools>`.

.. _2024_allowed_languages:

Allowed Programming Languages
-----------------------------

To build the firmware your design, your team may choose to implement your design
in any compatible language. To align with good development practices, you must
not use a language that was specifically chosen to be difficult to understand.

The pre-approved programming languages are: C, C++, and Rust. If you wish to use
a different language, please reach out to the organizers first.

The :doc:`../components/reference_design` was created in C, and as such if you
team decides to use another programming language, the Nix build environment must
be updated to allow for the organizers and teams to be able to build and utilize
your design.

