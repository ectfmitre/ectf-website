Functional Requirements
=======================

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

This section defines the functional requirements of your design. The
requirements are presented as a series of steps that define functionality your
system must implement. For each step, a Host Tool provided by the organizers and
run on a :ref:`2024_host_computer` will be invoked to interact with the
:ref:`2024_medical_device`.

The functionality described in each step will be verified by the organizers
during :doc:`../events/handoff`, and you must follow **all** requirements to
advance to the :ref:`attack_phase`.

See :doc:`../components/reference_design` for details about how the
MITRE-provided Reference Design implements the Functional Requirements.

.. note::
    **The functional requirements described here are intentionally high level**.
    See the :doc:`detailed_specs` for detailed requirements including tool
    inputs, outputs, and environments.

.. _2024_build_misc:

Build MISC System
-----------------
The complete system is built in two parts. The :ref:`2024_components` are built at
the secure component facility while the :ref:`2024_application_processor` (AP) is
built at the medical device manufacturer's facility, where it is assembled into
a final device. The build process for the Components and the AP is shown below.


.. figure:: ../../_static/MISC_Build.svg
    :align: center
    :scale: 50 %

This build process is broken into multiple steps:

* :ref:`2024_build_environment`
* :ref:`2024_build_deployment`
* :ref:`2024_build_aps_and_comps`

Your team must implement each of these steps to create a full MISC system.

.. _2024_build_environment:

Build Environment
^^^^^^^^^^^^^^^^^

Build Environment is where all of your projets dependencies are installed
including compilers, packages, and any other build tools you  may need. These
dependencies must be installed utilizing :doc:`../getting_started/nix`. This
step will be run once for each design, such that the successive build stages can
run as intended.

The environment is built by invoking ``nix-shell``, which will import the Nix
packages deefined in ``shell.nix`` and run any additional shell commands to
create the environment. You may customize the environment build process by
modifying ``shell.nix``. Nix offers many packages out of the box (e.g., Python,
GCC) and you may download and install additional custom packages, repositories,
and and files as needed. See :doc:`../getting_started/nix` for an example of
both.

.. note::
    If you don't change the programming language of your design or add
    additional build tools, it is possible that the Build Environment step of
    the :doc:`../components/reference_design` will be sufficent for your
    design without modification.

.. _2024_build_deployment:

Build Deployment
^^^^^^^^^^^^^^^^

Next, you will create a deployment that represents an entire fabrication run of
components created by the component manufacturer for the medical client. During
this step, ``deployment/Makefile`` is invoked by the Build Deployment
:doc:`../components/host_tools`, which may generate initial data required for
building and provisioning the system.

The resulting files are known as the Global Secrets, because they are provided
to the subequent build processes of the :ref:`2024_application_processor` and
:ref:`2024_components`. The Global Secrets may include cryptographic key material,
seeds, entropy, or any other data needed to build your design. Global Secrets
are **read only** after generation; the AP and Component phases of the build
process must not add new secrets to the Global Secrets.

APs and Components built using the same Global Secrets are known as a
"Deployment." APs and Components within a validly-built Deployment must be able
to work together as a :ref:`2024_medical_device`. The behavior of a Medical Device
comprising APs and Components from different is undefined and will not be
tested.

During the Attack Phase, one Deployments will be built for each scenario (see
:doc:`../flags/attack_flags`). Attackers will never have access to the Global
Secrets.


.. _2024_build_aps_and_comps:

Build Application Processors and Components
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once the environment and Deployment has been built, you may now build the
:ref:`Application Processors<2024_application_processor>` and :ref:`2024_components`.
Each is done by using the relevant :doc:`Host Tool<../components/host_tools>`.
APs and Components must be able to be built in any order and may read from the
Global Secrets as needed. The buid configuration for Components will include
some Attestation Data that must be stored on the device.


Create a Medical Device
-----------------------

Once the firmware for an AP and two Components are built, they may now be turned
into a Medical Device. First, each must be flashed (installed) onto the three
provided development boards using the :ref:`2024_update_tool`. The boards should then
be inserted into the provided custom carrier board PCB and each connected by USB
to the :ref:`2024_host_computer`. Once powered, the Medical Device should now be
operational and should listen for commands from the
:doc:`../components/host_tools`.


.. _2024_misc_func_reqs:

MISC Functional Requirements
----------------------------

With a fully assembled Medical Device, the system should now be ready to accept
commands from the :doc:`../components/host_tools` to the
:ref:`2024_application_processor`. The MISC that you design must respond to the
following five commands. Detailed requirements of these commands are listed
in the :doc:`detailed_specs`.

.. image:: ../../_static/MISC_HostTools.svg
    :align: center


List Components
^^^^^^^^^^^^^^^
The MISC must be able to list the Component IDs of the Components currently
installed on the Medical Device. This command is not authenticated and may be
initiated by anyone, as the Component IDs are not sensitive data.

Attest
^^^^^^
To debug and validate the integrity of the Components, the MISC must allow an
authorized user to retrieve the Attestation Data that was stored on the
Components during the build process. This should only occur if the user is able
to provide a valid Attastation PIN (see :doc:`security_reqs`).

Replace
^^^^^^^
In the case that one of the Components fails, the MISC should allow an
authorized user to replace it with a new, valid component. This should only
occur if the user is able to provide a valid replacement token and if the new
Component is authentic (see :doc:`security_reqs`).

Boot
^^^^
One of the most important functionalities of the MISC is to boot the Medical
Device. During this process, the MISC must first ensure the integrity of the
device and the Components on it (see :doc:`security_reqs`). If this integrity
check fails, the boot process will be aborted. Otherwise, the MISC should print
a boot message and hand off control of the AP and Controllers to the target
software that will then run the Medical Device.

Secure Send & Receive
^^^^^^^^^^^^^^^^^^^^^
After a successful boot, the MISC must provide a secure communciations channel
for the AP and Components to use. This channel will allow the AP and Components
to securely send and receive messages (see :doc:`security_reqs`).
