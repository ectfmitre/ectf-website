System Architecture
===================

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

Your team's design will consist of a :ref:`2024_host_computer` and a
:ref:`2024_medical_device` comprised of an :ref:`2024_application_processor`, and two
:ref:`2024_components`. The MISC software you design will protect the Medical Device
from supply chain attacks. The image below shows a high-level overview of the
system architecture.

.. image:: ../../_static/MISC_Architecture.svg
    :align: center


.. _2024_host_computer:

Host Computer
-------------

The host computer is a general-purpose computer (i.e., your laptop or desktop)
used to communicate with the :ref:`2024_medical_device` over a serial interface
through a number of :doc:`../components/host_tools`. These tools will be used to
initiate the various functionals of the medical device (see
:doc:`functional_reqs`) and to read back status messages and data. To simplify
the sytem, all Host Tools will be written by the organizers. In addition,
:doc:`../getting_started/nix` will be used to create reproducible build
environments across platforms.

.. _2024_medical_device:

Medical Device
--------------

The main focus of your team's work is on the Medical Device. A Medical Device is
comprised of an :ref:`2024_application_processor` (AP) and two :ref:`2024_components`
connected by a shared I2C :term:`bus`. Each will be implemented as an Analog
Device MAX78000FTHR development board connected by the custom :term:`PCB`. MISC
will be used to protect multiple types of medical devices (e.g., infusion pumps),
so your design shouldn't care about what the final device will be used for.



.. _2024_application_processor:

Application Processor
---------------------
The Application Processor (AP) is the "brains" of the medical device. It handles
all of the communications with the :ref:`2024_host_computer`, orchestrates the MISC
funtionality that you must implement (see :doc:`functional_reqs`), and conducts
all of the processing necessary to operate the device during its normal
operation after boot that other teams at your company will implement. As part of
the MISC protocol, the AP is responsible for ensuring the integrity of the
device (see :doc:`security_reqs`).


.. _2024_components:

Components
----------

The Components represent the many additional chips that may be found on a
medical device including sensors that take measurements and actuators that
interact with the patient. The components rely on the AP to ensure the integrity
of the device.


Development Resources
---------------------

Teams will be provided the following resources:

- **3 un-keyed MAX78000FTHR boards (the development boards)**
    - These boards will be used for the development of your design. Instructions
      will be provided to run the host tools on a local computer to test the
      entire system using the physical hardware. These devices will not be able
      to run :ref:`attack_phase` designs provisioned by the eCTF
      organizers. However, the development microcontrollers can be used to
      practice attacks against designs in the Attack Phase that are compiled
      locally by the team from source.

- **3 keyed MAX78000FTHR boards (the attack boards)** 
    - These boards will be used for the Attack Phase to securely the load other
      teams' designs that will be provided by the eCTF organizers. These devices
      are configured for use in the Attack Phase and therefore will be unusable
      during the Design Phase. *These boards will be distributed to teams as*
      :doc:`../events/handoff` *nears.*

- **eCTF Breakout Board** 
    - Breakout board that connects the I2C bus between the MAX780000 boards. 

- **USB Hub** 
    - The USB Hub is used for connecting all 3 MAX78000FTHR boards to your host
      computer.
