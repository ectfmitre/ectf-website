Fortinet Training
=================

`Educational Challenge: Threat Hunting Workshop to Identify Adversarial Behaviors <https://training.fortinet.com/local/staticpage/view.php?page=fast-track-workshop-abstracts>`_

**The space restriction for the Fortinet training has been lifted, so all
competitors who RSVP are invited to attend.**

On April 25, Diamond Sponsor Fortinet will be hosting a training at the MIT
Hacker Reactor Space in Cambridge, MA from 1-5PM ET the day before the
:doc:`award_ceremony`. 

Fortinet invites you to join the Threat Hunting Workshop to identify the
adversarial behaviors using renowned frameworks and procedures. Adopt a
proactive approach that involves hunting for threats based on the Tactics,
Techniques and Procedures (TTPs) that threat actor's use. 

Assume the role of a security analyst and be asked to identify any undetected
threats on a fictitious network. To do this you will make use of MITRE ATT&CK™,
which is a knowledge base of adversary behavior based on real-world
observations.

Students will gain hands-on experience developing and understanding the
analytics needed to discover the techniques used by adversaries during a cyber
security breach.

Participants who attend this workshop will learn:

- What is the MITRE ATT&CK framework and how it can be used
- What are the TTPs that threat actors use to carry out a breach
- How to use EndPoint Detection & Response's Threat Hunting capabilities to
  uncover threats on the network
- How to use SIEM's analytics to discover attacker behavior based on attack
  techniques
- How to use deception technology to find attacker activity and shorten attacker
  dwell time

**CISSP Credits:** 4


Pre-requisites (strongly encouraged):

- `Fortinet Certified Fundamentals - Getting Started in Cybersecurity <https://training.fortinet.com/course/index.php/Certification:FCF_Cybersecurity>`_
  (create a login, no cost)

    - Lesson 3: Sandbox
    - Lesson 8: Endpoint Hardening Techniques
    - Lesson 9: Endpoint Monitoring
    - Lesson 11: FortiSIEM

- Fortinet Certified Fundamentals - Technical Introduction to Cybersecurity

    - Module 2: Secure Network `Session 11 FortiSandbox <https://training.fortinet.com/course/view.php?id=39409>`_
    - Module 5: Endpoint Security

- `FortiDecepter in 5 minutes <https://www.youtube.com/watch?v=ygb4kpEVpGY>`_
