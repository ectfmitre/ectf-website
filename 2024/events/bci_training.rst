BCI Training
============

On March 15-17, eCTF sponsor Boston Cybernetics Institute
(https://www.bostoncyber.org/) will host a remote workshop for all registered
eCTF students and advisors titled **Embedded Reversing and Exploitation**.

This is a 3-day, majority hands-on course with minimal lecture. The course
begins with disassembly, decompiling, emulation, and debugging. We then focus on
vulnerability discovery and binary exploitation, giving students the ability to
hijack control of embedded targets.

The course is March 15th - 17th and delivered remotely over Zoom and Discord.
All necessary software is provided via a Linux VM. You will need VirtualBox or
VMWare and a computer that can run an x64 VM.

Zoom link: https://us02web.zoom.us/j/86946018481?pwd=Ym5lNnYwR3lNSmZ0eWppSTRyb0JSdz09

Discord server: https://discord.gg/XDEAPEpC

**Before you join the class:**

- Download and setup the VM (~2 hours)

    - The class VM is an ova file, available here: Shared
    - File is 15GB compressed, 35GB when expanded and running
    - Import OVA file to VirtualBox/VMWare
    - Do not wait until class begins to verify that your VM is working
      correctly. You should check internet connection, set up shared folders,
      adjust processors/RAM, etc.
    - Take snapshot of VM to preserve working state
    - Additional class materials will be distributed each day

No experience necessary. Materials are structured and paced to challenge
everyone from beginners to seasoned professionals.

See you soon and happy hacking.
