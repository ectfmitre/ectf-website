Schedule
========

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

Phases
^^^^^^
* :ref:`design_phase` Teams design and build their own design
* :ref:`handoff`: Teams turn in their design to the eCTF organizers for testing
* :ref:`attack_phase`: Teams that have passed Handoff can earn points by
  attacking the designs of other teams in the Attack Phase

.. image:: ../../_static/eCTF\ Phases.png

.. _2024_important-dates:

Important Dates
^^^^^^^^^^^^^^^
* January 17: Competition Kickoff, :ref:`design_phase` begins
* February 28: :ref:`handoff` begins
* April 17: :doc:`../../about/scoreboard` closes, :term:`flags <flags>` no
  longer accepted
* April 24: :doc:`poster_session`
* April 26: :doc:`award_ceremony`

Design Phase Flags Due Dates
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
See :doc:`../flags/design_flags` for details.

* January 24: Read the Rules
* January 26: Boot Reference
* January 31: Initial Design Document
* February 7: Use Debugger
* February 14: Testing Service

Workshop Schedule
^^^^^^^^^^^^^^^^^
The eCTF organizers will hold routine workshops to provide competitors with
information about different parts of the competition. Monitor the announcements
channel on :doc:`../../../about/slack` for details.

* January 18: Rules & Reference Design Overview
* January 23: Boot Reference Design
* January 25: Design Document
* February 1: Debugger Walkthrough
* February 8: Testing Service
* February 15: Development Help
* February 22: Handoff Preparation
* February 29: Handoff Help
