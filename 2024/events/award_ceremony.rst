2024 Award Ceremony
===================

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

This year, the Award Ceremony will be held on April 26 in the Boston area.
There will be opportunities to apply for travel grants. More information will
be announced soon on :doc:`../../../about/slack` and updated here.