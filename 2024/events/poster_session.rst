Poster Session
==============

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

On April 24, all teams will be invited to a virtual Poster Session featuring
posters created by your fellow competitors. Teams submitting posters will be
eligible to receive points for the quality of the poster and demonstration as
judged by a panel of subject matter experts from MITRE and the eCTF sponsors.

More information about the details of the poster session, submitting posters,
and scoring will be announced on :doc:`../../../about/slack` and updated here.
