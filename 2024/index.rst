2024 eCTF
=========

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

Welcome to the 2024 eCTF!

This year, teams will teams will design and implement a supply chain security
solution for microcontrollers on a medical device. The system must securely
verify the device integrity while keeping sensitive data confidential.

You are a member of a leading chip manufacturer's embedded systems security 
team. One of your customers, a medical device company, has disclosed concerns 
around counterfeit versions of components that your company sells. To assuage 
their concerns and protect your company's reputation, your team has been 
tasked with developing a system to verify the authenticity and integrity of 
your chips on medical device end products called the Medical Infrastructure 
Supply Chain (MISC).

The typical lifecycle of one of your companies' components is shown in 
:ref:`the image below <2024motivational_scenario_image>`. Your team will 
provide the tools to build components at the secure production facility, 
build the application processor at the medical device production facility, 
and tools to interact with the device both in operation for patients 
and at the repair facility. While your components must still work accurately 
inside of the customers devices, they must also provide mechanisms to protect 
the integrity of the system from attackers.

*You and your team have been tasked with designing and building a system that
solving this problem! How will you enable safe computing for patients globally?*

.. _2024motivational_scenario_image:

.. image:: ../_static/MISC_MotivationalScenario.svg
    :align: center


.. raw:: html
    
    <br />


.. image:: ../_static/2024_sponsors.png
    :align: center


.. admonition:: Tip

    We recommend starting with reading :doc:`getting_started/ectf_guide` to
    help your team get started off on the right foot.

The following pages will outline the details of the competition.


.. toctree::
    :maxdepth: 2
    :caption: Getting Started:

    getting_started/index

.. toctree::
    :maxdepth: 1
    :caption: Schedule and Events

    events/schedule
    events/testing_service
    events/handoff
    events/poster_session
    events/award_ceremony
    events/debugger_flag

.. toctree::
    :maxdepth: 1
    :caption: Competition Specifications:

    specs/system_architecture
    specs/functional_reqs
    specs/security_reqs
    specs/detailed_specs

.. toctree::
    :maxdepth: 1
    :caption: Competition Components:

    components/reference_design
    components/host_tools
    components/bootloader

.. toctree::
    :maxdepth: 1
    :caption: Flags and Points:

    flags/design_flags
    flags/attack_flags
    flags/documentation_points
