Reference Design
================

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

The eCTF 2024 reference design is provided to give teams a starting example that
meets all of the :doc:`../specs/functional_reqs`. This design does not implement
**any** security, and as such does not meet any of the
:doc:`../specs/security_reqs`.

Unlike previous years, the eCTF 2024 reference design and eCTF 2024 tools live
within the same reference design repository.

You can find the 2024 reference design at
https://github.com/mitre-cyber-academy/2024-ectf-insecure-example
    

Build Environment Implementation
--------------------------------

During the :ref:`2024_build_environment` step, the reference design installs the
dependencies required for building and interacting with the design utilizing
:doc:`../getting_started/nix` and :doc:`../getting_started/poetry`. 

For Nix, the reference design installs:

* Make
* Python3.9
* gcc-arm-embedded
* Poetry
* Cacert
* Minicom
* Analog Devices MSDK
* OpenOCD - Uncomment line in ``shell.nix`` to enable

Optionally, the example design also handles installation of Analog Devices
OpenOCD with the use of a custom Nix package. Information on how to utilize
OpenOCD + GDB to debug your teams design is available at
:doc:`../getting_started/openocd`.

For Poetry, the reference design installs:

* PySerial
* ArgParse
* Loguru
* GDBGUI

These dependencies will act as a good starting point for your team to implement
your design, but may not be sufficient on their own. Get to know the tools to
add dependencies in both Nix and Poetry while building your design.

.. admonition:: Tip

    If you need to customize the environment and are running into problems
    with Nix or Poetry, ``#tech-support`` on :doc:`../../../about/slack` is a
    great place to find support

Build Deployment Implementation
-------------------------------

The reference design does not implement any security. As such, no real Global
Secret generation happens during the :ref:`2024_build_deployment` phase of the
reference design.

The reference design does show how to implement Global Secret generation through
the ``Makefile`` present in the deployment directory. This ``Makefile`` is
invoked on each run of ``ectf_build_depl`` and is needed to run in order to
build the firmware of the :ref:`2024_application_processor` (AP) and the
:ref:`2024_components`.


Reference Design Utility Libraries
----------------------------------

The Reference Design implements a set of utility libraries to help your design.
They wrap around more complicated, low-level hardware and software interfaces to
provide a simple interface to more complicated underlying behaviors. **We highly
recommend teams that are new to the eCTF start by using these high-level
libraries.**

.. warning::
    These libaries are provided to provide an easy way to handle common
    functionality. However as with the rest of the Reference Design, they
    provide no security guarantees.

Host Messaging Library
^^^^^^^^^^^^^^^^^^^^^^

Host messaging is a high-level library that handles formatting messages between
the AP and the host tools. More information on the structure of these message is
available in :ref:`2024_misc_interface`.

Utilizing the host messaging library is a simple way to ensure that all messages
are formatted correctly.

The Host Messaging interface and implementation can be found in
``inc/host_messaging.h`` and ``src/host_messaging.c`` in the
``application_processor/`` directory of the Reference design.

Board Link Library
^^^^^^^^^^^^^^^^^^

The board link library is a high-level library to send messages of arbitrary
data between the AP and the Components. This library uses the
:ref:`2024_simple_i2c` under the covers to send data across the I2C connection
between the AP and component.

The Board Link interface and implementation can be found in ``inc/board_link.h``
and ``src/board_link.c`` in both the ``application_processor/`` and the
``component/`` directories of the Reference design.

.. _2024_simple_i2c:

Simple I2C Library
^^^^^^^^^^^^^^^^^^

The simple I2C peripheral library is a high-level library that handles I2C
communication with the hardware on the MAX78000. This library creates a fully
asynchronous communication interface.

Data is sent and received through a set of arrays that are intended to simulate
physical registers on an I2C attached sensor.

Data can be sent and received directly by reading and writing values within
these registers.  This functionality is wrapped in the board link library,
unless additional features are required, the direct utilization of this library
should not be needed.

I2C uses a controller peripheral structure, where all transactions are initiated
by the controller.  Multiple devices can be connected to the same bus, and to
differentiate between devices a 7-bit address is utilized. The Reference Design
uses the Component ID to address the Components.

The Simple I2C interface and implementation can be found in
``inc/simple_i2c_controller.h`` and ``src/simple_i2c_controller.c`` of the
``application_processor/`` directory for the AP and
``inc/simple_i2c_peripheral.h`` and ``src/simple_i2c_peripheral.c`` of the
``component`` directory in the Reference Design.

.. note::
    I2C communications may not work how you might expect. Only one device on
    the :term:`bus` may initiate communications (the AP, in the Reference
    Design), which means the Components cannot send messages directly to the
    AP and must instead queue up a message in its registers for the 

.. _2024_simple_flash:

Simple Flash Library
^^^^^^^^^^^^^^^^^^^^

The simple flash library creates a high-level library for interfacing with the
flash memory present on the MAX78000FTHR. This interfaces allows you to read,
write, and erase flash memory.

:term:`Flash Memory` does not work in the same fashion as RAM that you may be
used to working with.  Flash memory can only be written in one direction (e.g.,
1 to 0). As such, in order to rewrite the same location in flash memory, the
memory must be erased (resetting the memory to all 1s). Flash is erased in bulk
units called erase blocks. For the MAX78000 platform, Flash can be erased in 8KB
segments.

The Simple Flash interface and implementation can be found in
``inc/simple_flash.h`` and ``src/simple_flash.c`` in both the
``application_processor/`` and the ``component/`` directories of the Reference
design.

.. note::
    :term:`Flash Memory` does not work like most memory you are probably used
    to working with, so make sure you use the Slimple Flash Library to interact
    with it.

.. _2024_crypto_interface:

Simple Crypto Library
^^^^^^^^^^^^^^^^^^^^^

The simple crypto interface creates a high-level library for performing
cryptographic operations through WolfSSL. This interface provided a hash
operation, and symmetric encryption capabilities. **We highly recommend that
teams new to the eCTF start by only using the Simple Crypto Library until they
have a working design.** You may choose to use other algorithms and make a more
complicated protocol after (you may call directly into the WolfSSL library like
the Simple Crypto Library does), but teams new to the eCTF frequently start with
overly ambitious designs and run out of time to implement them. 

.. admonition:: Tip

    A team that makes reasonable tradeoffs of security and simplicity to make
    it into the Attack Phase will do far better and have a much better
    experience than a team who gets caught up with perfection and runs out of
    time to finish, so start simple and work from there if you have time.