eCTF Bootloader
===============

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

The MITRE eCTF insecure bootloader is a unprotected version of the eCTF
bootloader.

This bootloader allows for the development of binaries that are compatible with
the protected version of the eCTF bootloader before entering into the attack
phase.

The :doc:`../components/reference_design` **only** works with the MITRE eCTF
bootloader, and you **should** utilize this bootloader to develop your design.

.. note::
    
    The MAX78000FTHR boards provided by MITRE **DO NOT INCLUDE** the eCTF
    insecure bootloader by default. Please download and flash this bootloader
    as specified below.

Download and Installation
-------------------------

:download:`Download the MITRE eCTF insecure bootloader here: insecure.bin. <../../_static/insecure.bin>`

To flash the MITRE eCTF insecure bootloader to your development boards, utilize
the *DAPLink* interface present on the MAX78000FTHR development boards. The
DAPLink interface shows up as a removable media source when plugged into a
computer. To flash the MITRE eCTF insecure bootloader, simply drag and drop the
file available above to the DAPLink drive. If installation is successful, you
should see a blue LED flash.

.. figure:: ../../_static/DAPLink_Interface.png
    :align: center

    **DAPLink Interface**

Usage
-----

The MITRE eCTF insecure bootloader consists of two modes, updating and running. 
When in updating mode, the onboard LED on the MAX78000FTHR will blink blue.
This mode can be entered by resetting the board while holding down either the 
*Bootloader Button* on the :doc:`../getting_started/ectf_breakout` or by holding down SW1
on the MAX78000FTHR.

.. figure:: ../../_static/max78000fthr_sw1.jpeg
    :align: center
    :scale: 15 %
    
    **MAX78000FTHR SW1**

When in updating mode, new firmware can be flashed through the eCTF `update` tool as discussed in 
:doc:`host_tools`.
