Host Tools
==========

.. warning::

    ARCHIVED PAST COMPETITION, FOR REFERENCE ONLY

The Host Tools allow teams to have a standard interface to the system
functionality detailed in :doc:`../specs/functional_reqs`. These include tools
to build the system as outlined in :ref:`2024_build_misc` (see :ref:`2024_build_tool_calls`
below) as well as tools to interact with functioning Medical Devices as outlined
in :ref:`2024_misc_func_reqs` (see :ref:`2024_misc_tool_calls` below).


.. _2024_build_tool_calls:

Build Tool Reference Calls:
---------------------------

The build tools are a set of scripts created by the organizers that execute the
required build stages as outlined in the :doc:`../specs/functional_reqs`. These
scripts are installed with the :doc:`../getting_started/poetry` environment
included with the example design.

The build tools will be utilized by the organizers when testing and provisioning
your design. As such, your design **must** build with these included tools.

These tools **cannot** be modified by your team and must work as included with
the example design.

The build tools can be executed from within the Poetry environment, or from
outside of the Poetry environment by running `poetry run` before the script
name.

.. figure:: ../../_static/MISC_Build.svg
    :align: center
    :scale: 50 %


Build Deployment
^^^^^^^^^^^^^^^^
This will run the `Makefile` found in the deployment folder using the following
inputs:

.. code-block:: bash

    ectf_build_depl --help
    usage: eCTF Build Deployment Tool [-h] -d DESIGN

    Build a deployment using Nix

    options:
      -h, --help            show this help message and exit
      -d DESIGN, --design DESIGN
                            Path to the root directory of the included design

Example invocation:

.. code-block:: bash

    ectf_build_depl -d ../ectf-2024-example

Build Application Processor
^^^^^^^^^^^^^^^^^^^^^^^^^^^
This will run the `Makefile` found in the application processor folder using the
following inputs:

.. code-block:: bash

    ectf_build_ap --help
    usage: eCTF Build Application Processor Tool [-h] -d DESIGN -on OUTPUT_NAME [-od OUTPUT_DIR] -p P -b BOOT_MESSAGE

    Build an Application Processor using Nix

    options:
      -h, --help            show this help message and exit
      -d DESIGN, --design DESIGN
                            Path to the root directory of the included design
      -on OUTPUT_NAME, --output-name OUTPUT_NAME
                            Output prefix of the built application processor binary Example 'ap' -> a
      -od OUTPUT_DIR, --output-dir OUTPUT_DIR
                            Output name of the directory to store the result: default: .
      -p PIN, --pin PIN     PIN for built application processor
      -t TOKEN, --token TOKEN
                            Token for built application processor
      -c COMPONENT_CNT, --component-cnt COMPONENT_CNT
                            Number of components to provision Application Processor for
      -ids COMPONENT_IDS, --component-ids COMPONENT_IDS
                            Component IDs to provision the Application Processor for
      -b BOOT_MESSAGE, --boot-message BOOT_MESSAGE
                            Application Processor boot message

Example invocation:

.. code-block:: bash

    ectf_build_ap -d ../ectf-2024-example -on ap --p 123456 -c 2 -ids "0x11111124, 0x11111125" -b "Test boot message" -t 0123456789abcdef -od build

Build Component
^^^^^^^^^^^^^^^
This wil run the `Makefile` found in the component folder using the following
inputs:

.. code-block:: bash

    ectf_build_comp --help
    usage: eCTF Build Application Processor Tool [-h] -d DESIGN -on OUTPUT_NAME [-od OUTPUT_DIR] -id COMPONENT_ID -b BOOT_MESSAGE -al
                                                 ATTESTATION_LOCATION -ad ATTESTATION_DATE -ac ATTESTATION_CUSTOMER

    Build an Application Processor using Nix

    options:
      -h, --help            show this help message and exit
      -d DESIGN, --design DESIGN
                            Path to the root directory of the included design
      -on OUTPUT_NAME, --output-name OUTPUT_NAME
                            Output prefix of the built application processor binary Example 'ap' -> ap.bin, ap.elf, ap.img
      -od OUTPUT_DIR, --output-dir OUTPUT_DIR
                            Output name of the directory to store the result: default: .
      -id COMPONENT_ID, --component-id COMPONENT_ID
                            Component ID for the provisioned component
      -b BOOT_MESSAGE, --boot-message BOOT_MESSAGE
                            Component boot message
      -al ATTESTATION_LOCATION, --attestation-location ATTESTATION_LOCATION
                            Attestation data location field
      -ad ATTESTATION_DATE, --attestation-date ATTESTATION_DATE
                            Attestation data date field
      -ac ATTESTATION_CUSTOMER, --attestation-customer ATTESTATION_CUSTOMER
                            Attestation data customer field

Example invocation:

.. code-block:: bash

    ectf_build_comp -d ../ectf-2024-example -on comp -od build -id 0x11111125 -b "Component boot" -al "McLean" -ad "08/08/08" -ac "Fritz"

.. _2024_update_tool:

Update Tool
^^^^^^^^^^^
Flashing the MAX78000 is done through the :doc:`bootloader`. You will need to
initially flash the bootloader onto the provided hardware. 

The eCTF bootloader needs to be in update mode to be flashable. The bootloader
is in update mode when the onboard LED is flashing blue. This mode can be
entered by holding the bootloader button while resetting the board.

.. warning:: 
    Only use .img files with the eCTF Insecure Bootloader!
    Other image types will not flash properly

.. code-block:: bash

    ectf_update -h
    usage: ectf_update [-h] -in INFILE --port PORT [-s]

    options:
      -h, --help            show this help message and exit
      -in INFILE, --infile INFILE
                            Path to the input binary
      --port PORT           Serial port
      -s, --secure          Update a secured or insecure firmware image

Example invocation:

.. code-block:: bash

    ectf_update -in example_fw/build/firmware.img --port /dev/ttyUSB0


.. _2024_misc_tool_calls:

MISC Tool Reference Calls
-------------------------

All host tools are provided by the organizers and create simplistic interfaces
for communication between the :ref:`2024_host_computer` and the
:ref:`2024_application_processor`. These tools define the uniform interface utilized
to meet the technical requirements on the AP. See :ref:`2024_misc_interface` for the
details of the protocol of the interface the Host tools use to communicate with
the  AP.


.. image:: ../../_static/MISC_HostTools.svg
    :align: center

List Tool
^^^^^^^^^

The list tool reports what components are provisioned as well as what components
are installed on the system. Since component IDs are public information, this
tool can help to determine the state of a provisioned application processor.

.. code-block:: bash

    ectf_list -h
    usage: eCTF List Host Tool [-h] -a APPLICATION_PROCESSOR

    List the components connected to the medical device

    options:
      -h, --help            show this help message and exit
      -a APPLICATION_PROCESSOR, --application-processor APPLICATION_PROCESSOR
                            Serial device of the AP

Boot Tool
^^^^^^^^^

The boot tool is utilized to boot the system. On success, the boot messages from
the components and application processor should be printed to the user.

.. code-block:: bash

    ectf_boot --help
    usage: eCTF Boot Host Tool [-h] -a APPLICATION_PROCESSOR

    Boot the medical device

    options:
      -h, --help            show this help message and exit
      -a APPLICATION_PROCESSOR, --application-processor APPLICATION_PROCESSOR
                            Serial device of the AP

Replace Tool
^^^^^^^^^^^^

The replace tool is used to replace a component on the system with another
component. This is a privileged tool and requires the replacement token to
execute. This tool must replace the specific component.

.. code-block:: bash

    ectf_replace --help
    usage: eCTF Replace Host Tool [-h] -a APPLICATION_PROCESSOR -t TOKEN -i COMPONENT_IN -o COMPONENT_OUT

    Replace a component on the medical device

    options:
      -h, --help            show this help message and exit
      -a APPLICATION_PROCESSOR, --application-processor APPLICATION_PROCESSOR
                            Serial device of the AP
      -t TOKEN, --token TOKEN
                            Replacement token for the AP
      -i COMPONENT_IN, --component-in COMPONENT_IN
                            Component ID of the new component
      -o COMPONENT_OUT, --component-out COMPONENT_OUT
                            Component ID of the component being replaced

Attestation Tool
^^^^^^^^^^^^^^^^

The attestation tool is used to return the confidential attestation data from a
component. This is a privileged tool and requires the attestation PIN to
execute. This tool must print out the attestation data from the specified
component.

.. code-block:: bash

    ectf_attestation --help
    usage: eCTF Attestation Host Tool [-h] -a APPLICATION_PROCESSOR -p PIN -c COMPONENT

    Return the attestation data from a component

    options:
      -h, --help            show this help message and exit
      -a APPLICATION_PROCESSOR, --application-processor APPLICATION_PROCESSOR
                            Serial device of the AP
      -p PIN, --pin PIN     PIN for the AP
      -c COMPONENT, --component COMPONENT
                            Component ID of the target component
