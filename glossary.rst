Glossary
========

.. note::

    If there are any terms that you had to learn in the eCTF that are missing
    from this list, please let the organizers know so we can add it

.. glossary::
    Attack Board
        During the :term:`Attack Phase`, teams will use a different type of
        board to load and run the designs of other teams. This uses a secure
        :term:`Bootloader` to securely load encrypted binaries, preventing teams
        from capturing flags through reverse engineering the firmware. These
        boards will also disable debugging access and may implement additional 
        restrictions. Teams will be mailed the attack boards when they near
        :term:`Handoff`. For the boards used in the :term:`Design Phase`, see
        :term:`Design Board`.

    Attack Flags
        See :term:`Flags`

        Flags captured during the :term:`Attack Phase`, proving that you have
        compromised the :term:`Security Requirements` of another team.

    Attack Phase
        See :ref:`attack_phase`

        The second phase of the competition where teams attack the designs of
        other teams. Teams move into the Attack Phase as soon as they pass
        :term:`handoff`, which means that each team will enter the Attack Phase
        at different times. 

    Authentication
        The principle that trusted parties should be able to verify their own
        identity to other parties.

    Bootloader
        A piece of software that is called early in the boot process to load
        other software, also frequently allowing the user to update the
        software. In the eCTF, the organizers have provisioned custom
        bootloaders on the design and attack boards. The boards used during the
        :ref:`design_phase` simply load the unencrypted binaries onto the board
        and hand off execution. The boards used during the :ref:`attack_phase`
        have their own secure bootloader that loads encrypted binaries onto the
        board.

    Bus
        A bus is a connection on or between computers that allows data to be
        transferred between components.

    Bug Bounty
        Points awarded for finding functional bugs in the
        :term:`Reference Design`. See :ref:`2025_bug_bounty` for details.

    Ciphertext
        Data that has been encrypted, as opposed to :term:`Plaintext`.

    Compiler
        A piece of software that takes written source code and turns it into a
        binary file that is readable by a computer. 

    Confidentiality
        The principle that data should not be readable by unintended parties.

    Cross-compiler
        A :term:`compiler` that compiles the program for an architecture
        different than the computer you are running it on. For example, the ARM
        Cortex-M4 architecture of the :term:`microcontroller` used for this
        competition is likely different from that of the CPU running your laptop
        or desktop.

    Crypto interface
        See :ref:`2025_crypto_interface`

        The :term:`Reference Design` comes with a basic interface to a
        cryptographic library that provides encryption, decryption, and hashing.
        This has been provided to ease the burden of finding and compiling a
        cryptographic library. Teams new to the eCTF are strongly encouraged to
        use this interface for their designs. However, **use with care as the
        eCTF organizers do not provide any guarantees on the security of the
        interface.**

    Defensive Points
        See :doc:`about/scoring_system`

        Points earned by a team for time spent in the :term:`Attack Phase` for
        :term:`Flags` that haven't been captured by any other team.

    Design Flags
        See :term:`Flags`

        Flags captured during the :term:`Design Phase` to help keep your team on
        track as you move towards :term:`Handoff`

    Design Board
        During the :term:`Design Phase`, teams will use development boards to
        load and run their own designs while they develop. These boards will
        only load unencrypted designs and allow debugging access. Teams will be
        mailed design boards after they register. For the boards used in the
        :term:`Attack Phase`, see :term:`Attack Board`.

    Design Phase
        See :ref:`design_phase`

        The first phase of the competition where teams design and build their
        secure designs.

    Flags
        Flags are the basic unit of many capture the flag competitions,
        including the eCTF. Flags in the eCTF are strings of characters in the
        format of ``ectf{FlagName_16HexChars}``. Some flags will be found in
        that format and can be submitted as is. Others may require formatting a
        solution into the flag. For example, if a challenge named ``PinExtract``
        requires you to find the pin of ``123456``, the flag will be
        ``ectf{PinExtract_123456}``. If the value of the solution is longer than
        16 characters, typically the scoreboard will accept both the full value
        and the first 16 characters of solution.

    Flash Memory
        See :ref:`2025_simple_flash`

        Flash memory is a common type of :term:`non-volatile memory <memory>`
        that allows the microcontroller to store data that persists across power
        cycles. Flash memory works differently than most memory you may be used
        to as it has three operations: reads, writes, and erases. Reads work
        like other memory and can read back the requested address. Writes,
        however, may only turn bits that are currently 1 to 0s. To turn 0s into
        1s, you must first issue an erase command to erase the entire page of
        flash memory - which turns the entire page to 1s - and then write the
        desired value.

        The :term:`Reference Design` has implemented a simple interface to flash
        memory that you may use to easily write to and erase flash.

    Functional Requirements
        See :doc:`2025/specs/functional_reqs`

        The specification of functionality your design must support. Designs
        will not pass :term:`handoff` without meeting all Functional
        Requirements.

    Git
        Git is a version control system that tracks code as you develop it and
        allows the coordination of multiple developers making changes to the
        same source code. The :term:`Reference Design` is distributed as a git
        repository and you are required to use git for your development so that
        the organizers can automate testing and design submission.

    Handoff
        See :doc:`2025/handoff/index`

        The process of a team submitting their design to the eCTF organizers at
        the end of the :ref:`design_phase`. Only designs that pass all
        functional testing and meet the rules of the competition will complete
        Handoff. 

    Handoff Package
        Once a design passes :term:`handoff`, the organizers will provide the
        full package of what will be given to attacking teams in the
        :term:`Attack Phase`. Teams must approve this package to move to the
        Attack Phase or restart the Handoff process after making any changes.

    Integrity
        The assurance of the accuracy of data, particularly relating to
        detecting or preventing accidental or malicious data manipulation.

    Memory
        The hardware that stores data on a computer. Memory can be volatile (the
        data is lost when the chip shuts down or reboots) or non-volatile (the
        data persists across reboots). Examples of memory include flash, NVRAM,
        DRAM, and SRAM. Memory may be on the same chip as the microcontroller
        (on-chip memory) or in its own chip on the :term:`pcb` (off-chip
        memory). Memory is frequently structured so that each byte or each group
        of bytes is assigned a fixed address so the :term:`processor
        <processor>` can easily read data from and write data to the memory
        (known as memory-mapped).

    Microcontroller
        A small computer on a single computer chip. In contrast to the CPUs on
        laptops, desktops, and servers, microcontrollers typically have far
        fewer resources and run at slower speeds.

    Offensive Points
        See :doc:`about/scoring_system`

        Points earned by a team in the :term:`Attack Phase` for capturing the
        :term:`Flags` of other teams.

    Processor
        The hardware part of a computer that executes the operations instructed
        by software.

    PCB
        A Printed Circuit Board (PCB) is the flat component that connects the
        individual chips and peripherals mounted onto it.
    
    Plaintext
        Unencrypted data, as opposed to :term:`Ciphertext`.

    Peripheral
        A peripheral is an additional device on a :term:`microcontroller` CPU,
        or :term:`pcb` that provides additional functionality to the device by
        allowing input into and/or output out of the device. Input devices
        include mouses and sensors, output devices include LEDs and speakers,
        and input/output devices including :term:`off-chip memory <memory>` and
        USB devices.

    Provisioned System
        A provisioned system is a set of files and devices provided by the eCTF
        organizers for use during the :ref:`attack_phase`. The provisioned
        system includes a set of binaries (protected by the organizers) that can
        be loaded with the :term:`secure bootloader <bootloader>` onto the
        Attack Phase boards to run a team's design. Unless explicitly stated,
        binaries distributed to teams in the Attack Phase will be encrypted to
        prevent attacking teams from trivially extracting flags without
        defeating the security requirements of the design.

    Poster Session
        See :doc:`2025/schedule/poster_session`

        During the Poster Session near the end of the competition, teams will
        have the opportunity to submit and present posters to their fellow
        competitors, MITRE orgagnizers, and competition sponsors. Teams may earn
        points for high quality posters.

    Reference Design
        The Reference Design is an implementation of the eCTF design provided by
        the organizers that implements the :term:`Functional Requirements` of
        the competition while making no attempt to implement any
        :term:`Security Requirements`. Teams may choose to use the Reference
        Design as a starting point or to toss it out and start from scratch,
        however we highly recommend teams new to the eCTF to start with the
        Reference Design.

    Security Requirements
        See :doc:`2025/specs/security_reqs`

        The specification of security guarantees your design should support.
        Teams will earn points in the :ref:`attack_phase` by compromising the
        Security Requirements of other teams' designs and by your own design
        withstanding the attacks of other teams.

    Testing Service
        See :doc:`2025/handoff/testing_service`

        The eCTF organizers will provide a service that allows teams to submit
        their designs on :doc:`about/slack` to be tested for meeting
        :term:`Functional Requirements`.
