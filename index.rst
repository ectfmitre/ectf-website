Welcome to the 2025 eCTF!
=========================

The Embedded Capture the Flag (eCTF) is an embedded security competition run by
MITRE that puts participants through the experience of trying to create a secure
system and then learning from their mistakes. The 2025 eCTF will run from
January 15 through April 16 with the Award Ceremony on April 25.

.. admonition:: Tip

    We recommend starting with reading :doc:`ectf_guide` to
    help your team get started off on the right foot.

.. _table-of-contents:

.. toctree::
    :maxdepth: 3
    :titlesonly:
    :caption: Table of Contents

    about/index
    ectf_guide
    2025/index
    rules
    faq
    glossary
    learning_resources
    changelog

.. toctree::
    :maxdepth: 2
    :titlesonly:
    :caption: Archive

    2024/index

