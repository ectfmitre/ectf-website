Frequently Asked Questions
==========================

What does X term mean?
----------------------
Terms relevant to the eCTF are defined in the :doc:`glossary`. If there is a
term that should be added, please notify the organizers and we can have it
added.

Is it OK to obfuscate our source code to make it more challenging to understand and attack?
-------------------------------------------------------------------------------------------
No. Obfuscations performed at compile-time (e.g., to make binary reversing more
challenging) are OK, but your source code needs to be written in a clear and
maintainable fashion. It should be well-commented and/or otherwise documented
clearly. Using programming languages purely for their difficulty to understand
is not allowed, and use of any language outside the :ref:`2025_allowed_languages` is
not allowed without prior organizer approval.

Can we add intentional delays during boot to make it more difficult for an attacker to collect large numbers of observations?
-----------------------------------------------------------------------------------------------------------------------------
There should not be any intentional delays that could push performance past the
:ref:`2025_timing_requirements`.

If your system detects that it is under attack, additional delays are OK, but
must be limited to no more than 5 seconds. If your system detects that it has
been put in an unstable scenario (e.g., voltage outside the operating range),
the system may reset itself and repeat the 5 second delay until the instability
is no longer present. Permanent lockouts or self-destruction are not allowed.

Can we attack another teams' development environment?
-----------------------------------------------------
No! Anything other than the provisioned devices, the host tools, and the
firmware images are considered out-of-bounds. In other words, there is nothing
that you may attack until your team enters the :ref:`attack_phase`.

.. warning::

    Attacking systems outside of what is allowed by the competition may violate
    state or federal laws

Is social engineering in-scope for this competition? Can we send phishing communications to other teams to trick them into revealing their secrets?
---------------------------------------------------------------------------------------------------------------------------------------------------
No, please don't do this. Keep your attacks technical. We love creative ideas,
but this one can easily violate state and federal laws.

Can we submit the reference design or a design with security that can be trivially defeated so we can move into the Attack Phase?
---------------------------------------------------------------------------------------------------------------------------------
No. As the eCTF is a design-build-attack-style competition, teams must submit a
design that exhibits significant effort. It is up to the discretion of the eCTF
organizers as to what level of modification counts as "significant effort" or
"trivial security", so please contact the organizers before submitting an
extremely pared down version of your design. 

.. admonition:: Tip

    You may submit designs up to the last day of the competition - and there
    have been very successful teams that did not submit on time - so don't panic
    if your design isn't ready on the first day of :ref:`handoff`.

Can we attack MITRE infrastructure or files that have been protected by MITRE for secure distribution of provisioned systems?
-----------------------------------------------------------------------------------------------------------------------------
No! Any infrastructure that has been created by MITRE is off-limits for this
competition, including the :term:`secure bootloader <bootloader>`. The eCTF
organizers put these capabilities in place to make the competition smoother for
everyone and should be considered transparent when attacking provisioned
designs. When submitting :doc:`2025/flags/attack_flags` to the scoreboard, the
attacking team must submit a brief summary of how the flag was captured to the
eCTF organizers. If capturing a flag involved any tampering with MITRE
infrastructure the flag points will not be awarded and teams may face penalties
or even disqualification.

