# 2025 eCTF
# Sphinx configuration
# Ben Janis
#
# (c) 2025 The MITRE Corporation

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = '2025 eCTF'
copyright = '2025 The MITRE Corporation. ALL RIGHTS RESERVED\nApproved for public release. Distribution unlimited 23-03630-1'
author = 'Sam Meyers, Ben Janis'
release = '2025.01.15'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.todo']

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

todo_include_todos = True
todo_emit_warnings = True


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
# html_theme = "sphinx_book_theme"
html_sidebars = {
    '**': [
        'about.html',
        'navigation.html',
        'relations.html',
        'searchbox.html',
    ]
}
html_theme_options = {
    "description": "The MITRE Embedded Capture the Flag Competition",
    "logo": "eCTF_wordmark_color.svg",
    "logo_name": False,
    "font_family": "'Trade Gothic',Helvetica,Arial,Lucida,sans-serif",
    "head_font_family": "Trade Gothic Bold Condensed,Helvetica,Arial,Lucida,sans-serif",
    "caption_font_family": "Trade Gothic Bold Condensed,Helvetica,Arial,Lucida,sans-serif",
    "body_text": "#0B2338",
    "anchor": "#88DEFF",
    "link_hover": "#005B94",
    "show_powered_by": False,
    "show_relbars": True,
}
html_static_path = ['_static']
html_favicon = 'favicon.png'

latex_logo = './_static/ME_eCTF_logo_large.png'
latex_use_xindy = True
latex_elements = {
  'preamble': r"""
\makeatletter
  \fancypagestyle{empty}{ % title
    \fancyhf{}
    \fancyfoot[C]{{\copyright 2025 The MITRE Corporation. ALL RIGHTS RESERVED. \newline Approved for public release. Distribution unlimited 23-03630-1}}
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{0pt}
  }
  \fancypagestyle{plain}{ % chapter page
    \fancyhf{}
    \fancyfoot[R]{{\py@HeaderFamily\thepage}}
    \fancyfoot[L]{{\copyright 2025 The MITRE Corporation. ALL RIGHTS RESERVED. \newline Approved for public release. Distribution unlimited 23-03630-1}}
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{0.4pt}
  }
  \fancypagestyle{normal}{ % normal
    \fancyhf{{}}
    \fancyfoot[R]{{\py@HeaderFamily\thepage}}
    \fancyfoot[L]{{\copyright 2025 The MITRE Corporation. ALL RIGHTS RESERVED. \newline Approved for public release. Distribution unlimited 23-03630-1}}
    \fancyhead[R]{{\py@HeaderFamily \@title, \py@release}}
    \fancyhead[L]{\py@HeaderFamily\nouppercase{\leftmark}}
    \renewcommand{\headrulewidth}{0.4pt}
    \renewcommand{\footrulewidth}{0.4pt}
  }
\makeatother
""",
    "extraclassoptions": "openany,oneside",
    "printindex": "",
}
